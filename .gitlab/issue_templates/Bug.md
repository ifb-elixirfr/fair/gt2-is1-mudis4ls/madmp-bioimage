**Notice : il y a plusieurs modèles de tickets ; faire le choix par le bias de la liste 'Choose a template'**

**Description du bug**
Une description claire et concise du bug.

**Comment le reproduire**
Étapes :
1. Aller à '...'
2. Clicker '...'
3. etc
4. Voir l'erreur


**Résultat attendu**
Une description claire et concise de ce qui était attendu.

**Captures d'écran**
Si possible et/ou pertinent, attacher ici des captures d'écran.
