**Notice : il y a plusieurs modèles de tickets ; faire le choix par le bias de la liste 'Choose a template'**

**La question**
Quelle est la question ?

**Précisions**
Soyez aussi précis que possible.

**Contexte supplémentaire**
Tout ce qui peut éclairer la nature de la question.
