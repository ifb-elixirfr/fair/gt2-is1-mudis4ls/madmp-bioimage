#!/bin/bash

grep -ohP "[a-f0-9-]{36}" * > temp$$
sort temp$$ > temp0$$
\rm temp$$

nd=$(uniq -d  temp0$$ | wc -l)
echo $nd

if [ $nd -ne 0 ]
then
    uniq -d  temp0$$ > temp$$
    for i in $(cat temp$$)
    do
        echo $i
    done
    \rm temp$$
fi
\rm  temp0$$
