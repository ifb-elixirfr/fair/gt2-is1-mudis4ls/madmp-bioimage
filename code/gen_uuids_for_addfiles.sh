#!/bin/bash


dir="/home/paule/ifb_sandbox/PGD/maDMP_bioimage/madmp-bioimage/questionnaire/"

for f in $(ls ${dir}/additional_files/*.csv)
do
    echo $f
    awk -f ${dir}/gen_uuids.awk $f > out$$
    mv out$$ $f
done
