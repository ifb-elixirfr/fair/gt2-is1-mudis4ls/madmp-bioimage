function is_uuid(str)
{
    if (str ~ /^[A-F0-9a-f]{8}-[A-F0-9a-f]{4}-[A-F0-9a-f]{4}-[A-F0-9a-f]{4}-[A-F0-9a-f]{12}$/)
    {
        return 1;
    }
    return 0;
}

function gen_uuid(     _uuid)
{
    if (("uuidgen" | getline _uuid) > 0)
    {
        uuid = _uuid;
    }
    else
    {
        uuid = "00000000-0000-0000-0000-000000000000";
    }
    close("uuidgen");
    
    return uuid;
}



BEGIN{}
{
    file_line = $0;
    nfields = split(file_line, a, " ");
    #print a[nfields];
    if (nfields > 0)
    {
        if (is_uuid(a[nfields]))
        {
            print $0;
        }
        else
        {
            print $0 " " gen_uuid();
        }
    }
}
