#!/bin/bash

readme="${PWD}/additional_files/README.md"

if [ "$#" -lt 2 ]
then
    echo "./make_dsw.sh <previous_version/from_scratch> <version> [prod]"
    exit 1
fi


# possible arguments are
# - from_scratch
# - x.y.z
# - x.y.z prod
#
# forbidden arguments (since either meaningless or we are past base version)
#
# - none from_scratch
# - none prod
# - prod
# - none


km_id="bioimage-pgd-structure-km"
previous_version=$1
version=$2

if [[ "$#" -eq 1 && $1 == "from_scratch" ]]
then
    previous_version="none"
    km_id="scratch-"${km_id}
    km_dir="${PWD}"
elif [[ "$#" -eq 3 && $3 != "prod" ]]
then
    km_id="dev-"${km_id}
    km_dir="${PWD}/dev-knowledge_models"
elif [[ "$#" -eq 3 && $3 == "prod" ]]
then
    km_dir="${PWD}/knowledge_models"
elif [[ "$#" -eq 2 ]]
then
    km_id="dev-"${km_id}
    km_dir="${PWD}/dev-knowledge_models"
fi

if [[ ${previous_version} != "none" ]]
then
    pversion_f1=$(echo ${previous_version} | cut -d'.' -f 1)
    pversion_f2=$(echo ${previous_version} | cut -d'.' -f 2)
    pversion_f3=$(echo ${previous_version} | cut -d'.' -f 3)
    
    re='^[0-9]+$'
    if ! [[ $pversion_f1 =~ $re \
                && $pversion_f2 =~ $re \
                && $pversion_f3  =~ $re ]]
    then
        echo "first argument is not a version number"
        exit 1
    fi
fi
if [[ ${version} != "none" ]]
then
    version_f1=$(echo ${version} | cut -d'.' -f 1)
    version_f2=$(echo ${version} | cut -d'.' -f 2)
    version_f3=$(echo ${version} | cut -d'.' -f 3)
    
    re='^[0-9]+$'
    if ! [[ $version_f1 =~ $re \
                && $version_f2 =~ $re \
                && $version_f3  =~ $re ]]
    then
        echo "second argument is not a version number"
        exit 1
    fi
fi


base_version="1.2.4"
# this means the migration mechanism is implemented only from
# v. 1.2.4; before that it has no meaning


version_greater_equal()
{
    printf '%s\n%s\n' "$1" "$2"  | sort --check=quiet --version-sort
}

if [[ ${previous_version} != "none" ]]
then
    version_greater_equal ${base_version} ${previous_version} 
    previous_compare_to_base=$?

    version_greater_equal ${previous_version} ${version}
    compare_versions=$?
fi
version_greater_equal ${base_version} ${version} 
compare_to_base=$? 

if [[ ${previous_version} != "none" ]] 
then
    if [[ $previous_compare_to_base == 1 ]]
    then
        echo "previous version must be 1.2.4 or higher;"
        echo "km migration not implemented before then"
        exit 1
    elif [[ $compare_to_base == 1 ]]
    then
        echo "version must be 1.2.4 or higher;"
        echo "km migration not implemented before then"
        exit 1
    elif [[ $compare_versions == 1 ]]
    then
        echo "current version must be ahead of previous version"
        exit 1
    fi
elif [[ $compare_to_base == 1 ]]
then
    echo "version must be 1.2.4 or higher;"
    echo "km migration not implemented before then"
    exit 1
fi

#csv_file="${km_dir}/template-fbi-embrc.csv"
csv_file="template-fbi-embrc.csv"

echo "process version ${version} on top of version ${previous_version} using ${csv_file}"

if [ ${previous_version} != "none" ]
then
    previous_type_layout_packages_file=${km_dir}/${km_id}"-"${previous_version}"-packages.csv_type_layout"
    previous_packs_file=${km_dir}/${km_id}"-"${previous_version}"-packages.json.packs"
else
    previous_type_layout_packages_file=${km_dir}/${km_id}"-"${previous_version}"-packages.csv_type_layout"
    cat /dev/null >  ${previous_type_layout_packages_file}
    previous_packs_file=""
fi




awk  -f make_dsw.awk \
     -v vprevious_version=${previous_version} \
     -v vversion=${version} \
     -v vreadme_file=${readme} \
     -v vkm_id=${km_id} \
     -v vprevious_packs_file=${previous_packs_file} \
     ${csv_file}  ${previous_type_layout_packages_file} \
     > tmp_string

if [ $? == 0 ]
then
    jsonf=$(head -1 tmp_string | cut -d" " -f 2)

    cat tmp_string
    rm tmp_string

    jsonlint-php ${jsonf}
    # this means that we assume that jsonf is a valid json file

    filename=$(basename ${jsonf} ".json")
    packages_file=${km_dir}/${filename}"-packages.json.packs"

    jq '{packages}' ${jsonf} > ${packages_file}

    csv_type_layout_packages_file=${km_dir}/${filename}"-packages.csv_type_layout"

    jq -c '[  .packages[]
      | .events[] ] | .[] '\
       ${jsonf} \
        | sed 's/{//g' \
        | sed 's/}//g' \
        | sed 's/\":\"/\",\"/g' \
        | sed 's/\"metricMeasures\":/\"metricMeasures\",/g'  \
        | sed 's/\"requiredLevel\":/\"requiredLevel\",/g'\
        | sed 's/\"tagUuids\":/\"tagUuids\",/g'\
        | sed 's/\"itemTemplateQuestionUuids\":/\"itemTemplateQuestionUuids\",/g'\
        | sed 's/\"changed\":/\"changed\",/g' \
        | sed 's/\"value\":\[/\"value\",\[/g' \
        | sed 's/\"chapterUuids\":\[/\"chapterUuids\",\[/g' \
        | sed 's/\"integrationUuids\":\[/\"integrationUuids\",\[/g' \
        | sed 's/\"metricUuids\":\[/\"metricUuids\",\[/g' \
        | sed 's/\"phaseUuids\":\[/\"phaseUuids\",\[/g' \
        | sed 's/\"questionUuids\":\[/\"questionUuids\",\[/g' \
        | sed 's/\"expertUuids\":\[/\"expertUuids\",\[/g' \
        | sed 's/\"referenceUuids\":\[/\"referenceUuids\",\[/g' \
        | sed 's/\"answerUuids\":\[/\"answerUuids\",\[/g' \
        | sed 's/\"choiceUuids\":\[/\"choiceUuids\",\[/g' \
        | sed 's/\"props\":\[/\"props\",\[/g' \
        | sed 's/\"description\":null/\"description\",\"\"/g' \
              > ${csv_type_layout_packages_file}

    cp ${jsonf} ${km_dir}/.

else
    cat tmp_string
    rm tmp_string
fi
 

