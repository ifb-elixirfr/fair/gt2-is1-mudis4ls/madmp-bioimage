#!/bin/bash

echo "bioimage-pgd-structure-km-1.2.4" > list_of_files_to_migrate.txt
echo "bioimage-pgd-structure-km-1.2.5" >> list_of_files_to_migrate.txt
echo "bioimage-pgd-structure-km-1.2.9" >> list_of_files_to_migrate.txt
echo "bioimage-pgd-structure-km-1.3.0" >> list_of_files_to_migrate.txt


for f in $(cat list_of_files_to_migrate.txt)
do
    json_file=$f."json.new.new.new"
    new_json_file=$f."json"

    mv $json_file $new_json_file
done

for f in $(cat list_of_files_to_migrate.txt)
do
    json_file=$f."json"
    packages_file=$f"-packages.json.packs"
    csv_type_layout_packages_file=$f"-packages.csv_type_layout"

    jq '{packages}' ${json_file} > ${packages_file}
    jq -c '[  .packages[]
      | .events[] ] | .[] '\
       ${json_file} \
        | sed 's/{//g' \
        | sed 's/}//g' \
        | sed 's/\":\"/\",\"/g' \
        | sed 's/\"metricMeasures\":/\"metricMeasures\",/g'  \
        | sed 's/\"requiredLevel\":/\"requiredLevel\",/g'\
        | sed 's/\"tagUuids\":/\"tagUuids\",/g'\
        | sed 's/\"itemTemplateQuestionUuids\":/\"itemTemplateQuestionUuids\",/g'\
        | sed 's/\"changed\":/\"changed\",/g' \
        | sed 's/\"value\":\[/\"value\",\[/g' \
        | sed 's/\"chapterUuids\":\[/\"chapterUuids\",\[/g' \
        | sed 's/\"integrationUuids\":\[/\"integrationUuids\",\[/g' \
        | sed 's/\"metricUuids\":\[/\"metricUuids\",\[/g' \
        | sed 's/\"phaseUuids\":\[/\"phaseUuids\",\[/g' \
        | sed 's/\"questionUuids\":\[/\"questionUuids\",\[/g' \
        | sed 's/\"expertUuids\":\[/\"expertUuids\",\[/g' \
        | sed 's/\"referenceUuids\":\[/\"referenceUuids\",\[/g' \
        | sed 's/\"answerUuids\":\[/\"answerUuids\",\[/g' \
        | sed 's/\"choiceUuids\":\[/\"choiceUuids\",\[/g' \
        | sed 's/\"props\":\[/\"props\",\[/g' \
        | sed 's/\"description\":null/\"description\",\"\"/g' \
              > ${csv_type_layout_packages_file}
done
