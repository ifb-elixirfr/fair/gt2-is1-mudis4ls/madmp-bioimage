# Modèle de PGD structure pour la bioimagerie

Création d'une trame de PGD (structure) ; une collaboration de l'IFB, le FBI et EMBRC-FR.

## Changelog

### 1.3.1

* upgraded to KM metamodelVersion 8
* phase and metrics creation implemented
* phase and metrics attributes for questions/answers NOT yet implemented

### 1.3.0

* complete reorganisation of DMP model: better structuration and question hierarchy
* also improved wording for DMP questions
* awk script can now read arrays 
* implemented ordering for ListQuestion (itemTemplateQuestionUuids)


### 1.2.7 to 1.2.9

* various bug fixes, and better formulation of DMP questions

### 1.2.6

* Implemented migration feature for move events

### 1.2.5

* Small fixes

### 1.2.4

* Reworking roles and responsibilities: better target list of roles to the context, i.e. the question asked
* Various fixes and code cleaning
* Prepare to allow for migration

### 1.2.3

* Introducing 'Answer FBI / EMBRC-FR', in csv template; but this is not actually used --as yet--. Only taken down as a note, for now.
* Nearly all issues in [here](https://github.com/froggypaule/maDMP-bioimage/issues) dealt with (except those requiring more thinking & work)

### 1.2.2

* Take into account various comments collated [here](https://github.com/froggypaule/maDMP-bioimage/issues)

### 1.2.1

* Set requiredLevel to 3  (before finishing the project) for all questions; better discrimination is planned for later

### 1.2.0

* Rewrite roles and responsibilities: this is only the first step; issues and comments in this respect haven't been dealt with as yet

### 1.1.0

* Clean awk generating script (more automation)
* Get rid of 'Commentaires': we use DSW's feedback feature instead

### 1.0.1

* Updated to `IFB:bioimage-pgd-structure-km:1.0.1` (Various fixes)

### 1.0.0

* Updated to `IFB:bioimage-pgd-structure-km:1.0.0` (Introducing multichoice questions)

### 0.0.0

* Initial version `IFB:bioimage-pgd-structure-km:0.0.0`
* Apologies for having started at 0.0.0 (!)

