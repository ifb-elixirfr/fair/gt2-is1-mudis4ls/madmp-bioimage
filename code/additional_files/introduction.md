Les objectifs de ce plan de gestion des données (PGD):

*   Accompagner les plateformes et leurs utilisateurs dans le cycle
    de vie de leurs données ;
*    PGD structure et PGD projet : nous proposons ici les champs
    à renseigner remplir pour le PGD structure en bioimagerie,
    accompagnés de recommandations ;
    ce PGD structure a pour but de venir renseigner
    (automatiquement, voir le prochain point)
    certains éléments des futurs PGDs projet des utilisateurs de la structure ;
*    Machine actionable data management plan (maDMP) :
    ce PGD est proposé dans un format qui se veut compatible avec
    le projet maDMP de l’INIST et de l’IFB, qui permettra de réutiliser
    et partager facilement les briques de description
    (par exemple pour la mise en place de l’espace de stockage
    d’un projet, pour nourrir le PGD d’un projet...),
    afin d’éviter la double saisie et s’assurer que
    les éléments pertinents sont connus des acteurs concernés.

  Le projet « PGD structure bioimagerie » : Le projet a pour
  objectif d’élaborer un PGD structure dont la base expérimentale
  est l’imagerie biologique. Dans ce contexte, la structure est définie 
  d'une part comme l'infrastructure qui est génératrice de données
  à la demande des projets de recherche, et d'autre part, comme l'entité qui
  est la garante de la pérénnité des données une fois le projet
  arrivé à son terme.
  
  Le PGD structure a pour vocation de servir ensuite de modèle à
  tout PGD de projet porté par l'infrastructure. Aussi, il est conçu
  comme un document évolutif et modulaire, destiné à s’enrichir de
  manière à répondre aux multiples demandes émanant des acteurs
  affectés  par la gestion des données de recherche.

  L’imagerie biologique est concernée par les différentes échelles pertinentes
  du vivant, nanométrique, micrométrique et au-delà,
  jusqu’aux organismes entiers, qu’il s’agisse de petits organismes
  modèles ou de petits animaux.
  L’information multidimensionnelle, spatiale
  et temporelle est rendue accessible à toutes ces échelles.
  La variété croissante des techniques d’acquisition d’images,
  pour l’essentiel par microscopie (photonique, électronique, champ proche),
  parfois combinées, la diversité de la nature des données images produites,
  de leurs formats initiaux, des informations recueillies, des traitements
  et analyses qui y sont appliqués, nécessitent une
  rationalisation de leur gestion.

  L’objectif du projet de « PGD structure bioimagerie », porté 
  par et réalisé conjointement avec
  les Infrastructure Nationale en Biologie Santé (INBS)
  « France-BioImaging » (FBI), « Institut Français de Bioinformatique » (IFB),
  et  « Centre National De Ressources Biologiques Marines »
  (EMBRC-France), est  d’élaborer une architecture de questions,
  la plus générique possible, destinée à moissonner les informations
  relatives aux données de recherche issues de l’imagerie biologique.
  Cette architecture formera le tronc du PGD projet à destination des
  usagers de cette structure, permettant accessoirement que les champs
  idoines du PGD projet soient automatiquement renseignés par les
  informations déjà glanées en amont.
  
  Le PGD structure s’adresse en priorité, mais non exclusivement, aux
  plateformes des l’INBS listées plus haut dont la mission est la mise à
  disposition des outils mutualisés d’imagerie, l'adhésion à des
  pratiques communes et standardisées allant de l’acquisition des données
  à leur interprétation, la garantie de la qualité des données, et
  la pérennité d'accès à ces données. Il se conçoit
  dans le cadre de la « science ouverte ».

  Le document qui suit décrit les éléments du cycle de vie de la donnée
  numérique, c’est-à-dire, les processus de création, d’identification,
  de documentation, de partage et d’archivage.
  Par souci de simplification pour cette première version du PGD structure,
  certaines caractéristiques constitutives sont imposées et
  certains types de données en sont exclus à ce stade.
  Néanmoins ce document, évolutif et destiné à être modulaire,
  s’enrichira en fonction des exigences et recommandations de diverses
  entités tutélaires, et des besoins émanant 
  des utilisateurs actuels et potentiels.
  Ce processus d’extension du PGD n’est pas encore formalisé à ce stade.

  L'objectif premier du PGD structure est d’accompagner les données de tous
  renseignements permettant ainsi de les rendre ainsi visibles, accessibles,
  et réutilisables quel que soit le demandeur. Cet objectif
  s’inscrit dans le paradigme FAIR (https://www.go-fair.org/fair-principles/),
  soit « Facile à découvrir, Accessible, Interopérable, Réutilisable »,
  qui est l’un des piliers de la science ouverte.
  
  Un autre objectif est l’automatisation de la collecte et
  de la diffusion des informations pertinentes aux données, afin
  d'optimiser ce processus fastidieux, de le rendre plus robuste en
  limitant l'intervention humaine, et, enfin, de
  garantir une visibilité maximale par
  l’exposition du cadre qui gouverne la réutilisation des données. 




