function gen_uuid(     _uuid)
{
    if (("uuidgen" | getline _uuid) > 0)
    {
        uuid = _uuid;
    }
    else
    {
        uuid = "00000000-0000-0000-0000-000000000000";
    }
    close("uuidgen");
    
    return uuid;
}

BEGIN{
    FS = ",";
    l = 0;
}
{
    l++;
    if (l == 1)
        # first line
    {
        print $0;
    }
    else
    {
        uuid = gen_uuid();
        printf("%s,%s,\"%s\",", $1, $2, uuid);
        
        for (i = 4; i < NF; i++)
        {
            printf("%s,", $i);
        }
        printf("%s\n", $NF);
    }
}
END{
}
