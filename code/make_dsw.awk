# for-fbi-embrc-xxx.csv
# and json creation for dsw

# this awk file is complex and is used to read quite a few files:
# - the main file (the input to awk on the command line)
#   - it contains the bulk of the DMP described in cvs file
#   - see BEGIN for the definition of the separator, or rather FPAT
#     - NEVER EVER use quotes " in this file, replace them with « or »
# - other files may be included (and read) at will:
#   - the "file" column is used for that purpose
#   - the files will be read using getline
#   - there maybe different types of files:
#     - *.md or *.txt files: those will used as dsw format text
#       (which is actually markdown)
#     - .csv files:
#       - to make simple, the separator for these files will be ","
#         why: because I think these files will be much smaller
#         and with quite 'simple', ie a simple tree content
#       - these files are automatically understood to contain
#         questions and answers: those will be coded exactly
#         as in the main csv file,
#         ie, prefixed with QuestionOptions, Option, etc...
# - how/where do these files (actually their content) fit
#   - for a .txt/.md file: the text will be served as a text or advice
#     for the CURRENT uuid (text if it is a question, advice if answer)
#   - as questions/answers to the CURRENT uuid
#     AND with level one larger than the CURRENT level
#   -> this NECESSARILY implies that one must be careful in which
#      row of the main csv file one includes the file in question
#   - recall that current uuid and current level are given
#    by uuid_stack, uuid_levels & last_uuid
#   - CAREFUL: recall then that the file column may be filled irrespective
#     of 'what' it applies to (ie the parent uuid)
#     - this means that checking this condition must be done in all
#       cases: ie is_file=true is NOT exclusive to is_chapter=true
#       or anything else



# assert -- assert that a condition is true. Otherwise exit.
# Arnold Robbins, arnold@gnu.ai.mit.edu, Public Domain
# May, 1993
function assert(condition, string,    _assert_exit)
{
    if (! condition)
    {
        printf("%s:%d: assertion failed: %s\n",
               FILENAME, FNR, string) > "/dev/stderr";
        _assert_exit = 1;
        exit 1;
    }
}
END{
    if (_assert_exit)
    {
        exit 1;
    }
}

function randint(n)
{
    return int(n * rand())
}

function strip_quotes_from_string(str)
# all the original cells are quoted strings, we must trim them (ie remove the
# start and end quote)
# just to make sure, we'll check this is the case
{
    if (length(str) > 2 && substr(str, 1, 1) == "\"")
    {
        str = substr(str, 2, length(str) - 2);
    }
    return str;
}
function get_quotes_around(str,  _newstr)
# this is to make sure we save newly filled cells (by this program) properly
# in the .csv file
# we make sure quotes are not already there
# and remember: there are no quotes to be found in any cells!
# (see preamble)
{
    if (substr(str, 1, 1) != "\"")
    {
        _newstr = "\"" str;
    }
    else
    {
        _newstr = str;
    }
    
    if (substr(str, length(str), 1) != "\"")
    {
        _newstr = _newstr "\"";
    }
    return _newstr;
}

function get_process_number(    _id)
{
    "echo $$" | getline _id;
    close("echo $$");
    return _id;
}

function get_iso8601_date_time(    _id)
{
    "date -Is" | getline _id;
    close("date -Is");
    return _id;
}

function get_cleaned_date_time(    _id)
{
    # attention: no colon in name for dsw: it gets totally confused
    # and doesn't even throw an error
    "date +%FT%H-%M-%S%Z" | getline _id;
    close("date --utc +%FT%H-%M-%S%Z");
    return _id;
}
    
function initialise_header_indexes(max_level, header_array,    _i, _header)
{
    header_array["phase"] = "";
    header_array["metric_abbreviation"] = "";
    header_array["metric_description"] = "";
    header_array["metric"] = "";
    header_array["chapter"] = "";
    header_array["text"] = "";
    header_array["advice"] = "";
    header_array["file"] = "";
    header_array["uuid"] = "";
    header_array["answer_FBI_EMBRC-FR"] = "";

    for (_i = 1; _i <= max_level; _i++)
    {
        _header = "level " _i;
        header_array[_header] = "";
    }
}

function set_header_indexes(header_array, str, i,     _level_header)
{
    switch (str)
    {
        case "Phase":
        {
            header_array["phase"] = i;
            break;
        }
        case "Metric":
        {
            header_array["metric"] = i;
            break;
        }
        case "Metric Abbreviation":
        {
            header_array["metric_abbreviation"] = i;
            break;
        }
        case "Metric Description":
        {
            header_array["metric_description"] = i;
            break;
        }
        case "Chapter":
        {
            header_array["chapter"] = i;
            break;
        }
        case "Text":
        {
            header_array["text"] = i; 
            break;
        }
        case "Option Advice":
        {
            header_array["advice"] = i; 
            break;
        }
        case "File":
        {
            header_array["file"] = i; 
            break;
        }
        case "Answer FBI / EMBRC-FR":
        {
            header_array["answer_fbi_embrc-fr"] = i; 
            break;
        }
        case "uuid":
            case "Uuid":
            {
                header_array["uuid"] = i;
                break;
            }
            case /Level/:
            {
                nf = split(str, a);
                assert(nf == 2, "nf == 2");
                # a part from the headers described above, there can only be
                # "Level x"-type headers
                _level_header = "level "a[2];
                header_array[_level_header] = i; 
                break;
            }
            default:
            {
                break;
            }
    }
}

function is_phase(header_array, table, row,      _i)
{
    _i = header_array["phase"];
    if (length(table[row, _i]) != 0)
    {
        return 1;
    }
    return 0;
}
function is_metric(header_array, table, row,      _i)
{
    _i = header_array["metric"];
    if (length(table[row, _i]) != 0)
    {
        return 1;
    }
    return 0;
}
function is_metric_abbreviation(header_array, table, row,      _i)
{
    _i = header_array["metric_abbreviation"];
    if (length(table[row, _i]) != 0)
    {
        return 1;
    }
    return 0;
}
function is_metric_description(header_array, table, row,      _i)
{
    _i = header_array["metric_description"];
    if (length(table[row, _i]) != 0)
    {
        return 1;
    }
    return 0;
}
function is_chapter(header_array, table, row,      _i)
{
    _i = header_array["chapter"];
    if (length(table[row, _i]) != 0)
    {
        return 1;
    }
    return 0;
}
function is_text(header_array, table, row,      _i)
{
    _i = header_array["text"];
    if (length(table[row, _i]) != 0)
    {
        return 1;
    }
    return 0;
}
function is_advice(header_array, table, row,      _i)
{
    _i = header_array["advice"];
    if (length(table[row, _i]) != 0)
    {
        return 1;
    }
    return 0;
}
function is_file(header_array, table, row,      _i)
{
    _i = header_array["file"];
    if (length(table[row, _i]) != 0)
    {
        return 1;
    }
    return 0;
}
function is_uuid(header_array, table, row,      _i)
{
    _i = header_array["uuid"];
    if (length(table[row, _i]) != 0)
    {
        return 1;
    }
    return 0;
}
function is_question(max_level, header_array, table, row,   _level)
{
    _level = get_level(max_level, header_array, table, row);
    if (_level > 0)
    {
        return _level;
    }
    return 0;
}

function get_level(max_level, header_array, table, row,     _header)
# we'll use this concept of level to manage our parent-child uuid
# relationships
#
# columns text, advice, answer, and file  do not have a level (nor an uuid)
# since they necessarily
# occur (or might occur) in conjunction with a question event
# 
# question events are tagged by 'Question...', 'Option', or 'Choice' tags  
#
{
    if (is_phase(header_array, table, row)              \
        || is_metric(header_array, table, row)          \
        || is_chapter(header_array, table, row))
    {
        return 0;
    }
    else
    {
        for (i = 1; i <= max_level; i++)
        {
            _header = "level " i;
            if (length(table[row, header_array[_header]]) != 0)
            {
                return i;
            }
        }
    }
    return -2;
    # we don't return -1 since it is km_level
    # but then, it's not that important; the important thing is to capture
    # the level of anything that is not a chapter, text, advice, file....
}

function get_question_type_and_label_from_tag(question_tag,
                                              array_type_label)
{
    # we need to find which type of question, or if it is an answer
    switch (question_tag)
    {
        case /QuestionValueString/:
        {
            array_type_label["question_type"]    \
                = question_value_string;
            break;
        }
        case /QuestionValueDate/:
        {
            array_type_label["question_type"]    \
                = question_value_date;
            break;
        }
        case /QuestionValueNumber/:
        {
            array_type_label["question_type"]    \
                = question_value_number;
            break;
        }
        case /QuestionValueText/:
        {
            array_type_label["question_type"]    \
                = question_value_text;
            break;
        }
        case /QuestionListOfItems/:
        {
            array_type_label["question_type"]    \
                = question_list_of_items;
            break;
        }
        case /QuestionOptions/:
        {
            array_type_label["question_type"]    \
                = question_options;
            break;
        }
        case /QuestionMultiChoice/:
        {
            array_type_label["question_type"]    \
                = question_multi_choice;
            break;
        }
        case /QuestionIntegration/:
        {
            array_type_label["question_type"]    \
                = question_integration;
            break;
        }
        case /Option/:
        {
            array_type_label["question_type"]    \
                = option_answer;
            break;
        }
        case /Choice/:
        {
            array_type_label["question_type"]    \
                = choice_answer;
            break;
        }
    }

    return 0;
}




# one must parse each cell to determine their type:
# actually this is only relevant for questions since there
# are three types of them: ListOfItems, QuestionValue, QuestionOptions
#
# note that vars in awk are globals, so those found in the function
# and not declared in the profile of the function below
# have been defined in END{}
# they are not declared in the profile because the list is large
# and doing so would result in an ugly function profile
#
# typically I declare all vars used in the function in the profile
# for legibility, even so they are globals (except those
# local to the functions)
function get_question_type_and_label(level, header_array, table, row,
                                     text_file_to_read,
                                     array_type_label,
                                     _ind, _elt, 
                                     _a, _nfields, _uuid, _i)
{
    _ind = "level " level;
    assert(length(table[row, header_array[_ind]]) != 0,
           "length(table[row, header_array[_ind]]) != 0");
    # why? because this was checked before calling this function
        
    _elt = table[row, header_array[_ind]];
    _nfields = split(_elt, _a, " ");

    # we need to find which type of question, or if it is an answer
    get_question_type_and_label_from_tag(_a[1], array_type_label);
   # print _elt;
    
    assert(_nfields > 1, "_nfields > 1");
    array_type_label["question_label"] = _a[2];
    for (_i = 3; _i <= _nfields; _i++)
    {
        array_type_label["question_label"]               \
            = array_type_label["question_label"] " " _a[_i];
    }
            
    #print array_type_label["question_type"];
    #print_enum(array_type_label["question_type"]);
    #print array_type_label["question_label"];
                
    # now we need to take care of additionnal text or advice
    # this row might contain

    # if there is also a file to read
    # it doesn't matter if there is no file to read:
    # handled by get_txt_file_content
    # json doesn't like multilines, so we trick it by using \n
    # which becomes \\n for awk escaping 
    array_type_label["question_text"]                   \
        = get_text(header_array, table, row)                    \
        "\\n" get_txt_file_content(text_file_to_read);
    array_type_label["answer_advice"]                   \
        = get_advice(header_array, table, row)                  \
        "\\n" get_txt_file_content(text_file_to_read);

    _uuid = get_uuid(header_array, table, row);
    # just in case there is a mistake in the table
    assert(_uuid != "", "_uuid != \"\"");
    array_type_label["uuid"] = _uuid;

    return 0;
}


# there is an assumption here: the content of the file concerns
# successive questions/answers to be included in ONE specific
# spot (given by level and effected
# by write_create_question_event)
function get_question_type_and_label_from_file( \
    file_line,
    array_type_label,
    _level, _elt, _a, _nfieldsa,
    _b, _nfieldsb, _tag_label, _tag, _text, _uuid, _i)
{
    # assumption
    assert(file_line != "", "file_line != \"\"");
    
    # these csv files have '^' as a separator
    _nfieldsa = split(file_line, _a, "^");
    # each file row contains
    # - a tag (giving type of question) followed by the question's label)
    # - a certain number of separators ',': they will indicate
    #   the level of the row in question
    #   or rather, the depth of the hierarchical tree
    #   ie, the same meaning as when level is processed from the main
    #   csv file, EXCEPT here the value is RELATIVE to the value
    #   of the level at the time of opening the file in question
    #   (not the main file here)
    assert(_nfieldsa >= 1, "_nfieldsa >= 1");
    
    # moreover, by design, the last 'cell' is the one containing the 
    # question/answer tag and its label possibly followed by an
    # accompagnying text (for a question: tag will be QuestionText)
    # or advice (for an answer: tag will be OptionAdvice)
    #
    # for example
    # ^ ^ ^ QuestionOptions blah
    # means that the question is at level + 4 with respect to the level
    # at the start of the file read
    # ^ ^ ^ ^ ^ Option blahblah is at level + 5
    # while
    # QuestionOptions blahblahblah is at level +1
    #
    # from this it follows that each file line does NOT have an identical
    # number of cells

    # get the level then first,
    _level = _nfieldsa;
    
    # we need to find which type of question, or if it is an answer/choice
    # as per the protocol, question/answer tag and label are separated by
    # space
    assert(_a[_nfieldsa] != "", "_a[_nfieldsa] != \"\"");
    _tag_label = _a[_nfieldsa];

    #print "line", file_line;
    #print "_tag_label", _tag_label;
 
    _nfieldsb = split(_tag_label, _b, " ");
    assert(_nfieldsb >= 2, "_nfieldsb >= 2");
    
    get_question_type_and_label_from_tag(_b[1], array_type_label);

    # next we must take care of the uuid: it is ALWAYS the last elt
    # on the line
    # and we assume it must be present
    _uuid = _b[_nfieldsb];

    assert(matches_uuid(_uuid), "matches_uuid(_uuid)");
    array_type_label["uuid"] = _uuid;
    
    # careful: there might NOT be a label for the question
    # for the answer yes, wouldn't make sense otherwise
    # (because an answer is necessarily part of a list of options,
    # while a conditionnal suite of options/questions may well require
    # a QuestionsOptions tag without a label
    array_type_label["question_label"] = "";
    for (_i = 2; _i < _nfieldsb; _i++)
    {
        if (_b[_i] ~ /QuestionText/ || _b[_i] ~ /OptionAdvice/)
            # we must break here, we have reached the additional
            # text/advice part
        {
            break;
        }
        array_type_label["question_label"]               \
            = array_type_label["question_label"] " " _b[_i];
    }

    if (!matches_uuid(_b[_i]))
    {
        _tag = _b[_i];
        _text = "";
        for (++_i; _i < _nfieldsb; _i++)
        {
            _text = _text " " _b[_i];
        }
        
        #print "line", file_line;
        #print _tag, _b[1];
        if (_tag ~ /QuestionText/)
        {
            assert(_b[1] ~ /Question/, "_b[1] ~ /Question/");
            array_type_label["question_text"] = transform_text(_text);
        }
        else if (_tag ~ /Option/)
        {
            array_type_label["answer_advice"] = transform_text(_text);
        }
        else
        {
            assert(_b[1] ~ /Choice/, "_b[1] ~ /Choice/");
        }
        #print array_type_label["question_type"];
        #print_enum(array_type_label["question_type"]);
        #print array_type_label["question_label"];
    }
    
    return _level;
}

function get_phase(header_array, table, row)
{
    if (is_phase(header_array, table, row))
    {
        return table[row, header_array["phase"]];
    }
    return "";
}

function get_metric(header_array, table, row)
{
    if (is_metric(header_array, table, row))
    {
        return table[row, header_array["metric"]];
    }
    return "";
}
function get_metric_abbreviation(header_array, table, row)
{
    if (is_metric_abbreviation(header_array, table, row))
    {
        return table[row, header_array["metric_abbreviation"]];
    }
    return "";
}
function get_metric_description(header_array, table, row)
{
    if (is_metric_description(header_array, table, row))
    {
        return table[row, header_array["metric_description"]];
    }
    return "";
}

function get_chapter(header_array, table, row)
{
    if (is_chapter(header_array, table, row))
    {
        return table[row, header_array["chapter"]];
    }
    return "";
}

function transform_text(text,      _text, _txt, _nfields, _i, _a)
{
    #print text;
    _nfields = split(text, _a, "Newline ");
    _text = _a[1];
    # json doesn't like multilines, so we trick it by using \n
    # which becomes \\n for awk escaping 
    for (_i = 2; _i <= _nfields; _i++)
    {
        _text = _text "\\n" _a[_i];
    }
    #print _text;
    return _text;
}

function get_text(header_array, table, row,     _text, _txt, _nfields, _i, _a)
{
    if (is_text(header_array, table, row))
    {
        return transform_text(table[row, header_array["text"]]);
    }
    return "";
}
function get_advice(header_array, table, row)
{
    if (is_advice(header_array, table, row))
    {
        return transform_text(table[row, header_array["advice"]]);
    }
    return "";
}
function get_filename(header_array, table, row)
{
    if (is_file(header_array, table, row))
    {
        return table[row, header_array["file"]];
    }
    return "";
}
function get_uuid(header_array, table, row,    _uuid)
{
    if (is_uuid(header_array, table, row))
    {
        _uuid = table[row, header_array["uuid"]];
        if (!matches_uuid(_uuid))
        {
            print "not an uuid " _uuid;
            assert(matches_uuid(_uuid), "matches_uuid(_uuid)");
        }

        return _uuid;
    }
    # just to make sure nothing but an uuid returns
    # (in case is_uuid returned false ie "")
    assert(matches_uuid(""), "matches_uuid(_uuid)");
}

function get_file_type(filename,         _fn, _ext)
{
    ("basename " filename) | getline _fn;
    close("basename " filename);

    # Delete longest match of '*.' from front of _fn
    cmd="fn="_fn " && echo \"${fn##*.}\"";

    #print cmd;
    cmd | getline _ext;
    close(cmd)

    return _ext;    
}

function get_txt_file_content_raw(filename, with_blank_lines,     _text, _line)
{
    _text = "";
    if (filename == "")
    {
        return _text;
    }
    # there is no need to check for filename's existence since
    # The  getline  command returns 1 on success, zero on end of file, and -1
    # on an error.
    # from the man page

    while ((getline _line < filename) > 0)
    {
        # ignore blank lines if required
        if (!with_blank_lines && _line != "")
        {
            # json doesn't like multilines, so we trick it by using \n
            # which becomes \\n for awk escaping 
            _text = _text "\\n" _line;
        }
        else if (_line == "")
        {
            _text = _text "\\n";
        }
        else
        {
            _text = _text "\\n" _line;
        }
    }
    close(filename);
    return _text;
}

function get_txt_file_content(filename)
{
    return get_txt_file_content_raw(filename, 0);
}
function get_txt_file_content_wblank_lines(filename)
{
    return get_txt_file_content_raw(filename, 1);
}

# note that vars in awk are globals, so those found in the function
# and not declared in the profile have been defined in END{}
#
# they are not declared in the profile because the list is large
# and doing so would result in an ugly function profile
#
# typically I declare all vars used in the function in the profile
# for legibility, even so they are globals (except those
# local to the functions)
function print_enum(enum)
{
    if (enum == question_list_of_items)
    {
        print "question_list_of_items";
        return 0;
    }
    else if (enum == question_options)
    {
        print "question_options";
        return 0;
    }
    else if (enum == question_multi_choice)
    {
        print "question_multi_choice";
        return 0;
    }
    else if (enum == question_value_string)
    {
        print "question_value_string";
        return 0;
    }
    else if (enum == question_value_number)
    {
        print "question_value_number";
        return 0;
    }
    else if (enum == question_value_date)
    {
        print "question_value_date";
        return 0;
    }
    else if (enum == question_value_text)
    {
        print "question_value_text";
        return 0;
    }
    else if (enum == question_integration)
    {
        print "question_integration";
        return 0;
    }
    else if (enum == option_answer)
    {
        print "option_answer";
        return 0;
    }
    else if (enum == choice_answer)
    {
        print "choice_answer";
        return 0;
    }

    return 1;
}

# note that vars in awk are globals, so those found in the function
# and not declared in the profile have been defined in END{}
#
# they are not declared in the profile because the list is large
# and doing so would result in an ugly function profile
#
# typically I declare all vars used in the function in the profile
# for legibility, even so they are globals (except those
# local to the functions)
function prepare_question_type_value(enum, key_value_pairs_array)
{
    if (enum == question_list_of_items)
    {
        key_value_pairs_array["\"questionType\""] = "\"ListQuestion\"";
        return 0;
    }
    else if (enum == question_options)
    {
        key_value_pairs_array["\"questionType\""] = "\"OptionsQuestion\"";
        return 0;
    }
    else if (enum == question_multi_choice)
    {
        key_value_pairs_array["\"questionType\""] = "\"MultiChoiceQuestion\"";
        return 0;
    }
    else if (enum == question_value_string)
    {
        key_value_pairs_array["\"questionType\""] = "\"ValueQuestion\"";
        key_value_pairs_array["\"valueType\""] = "\"StringQuestionValueType\"";
        return 0;
    }
    else if (enum == question_value_number)
    {
        key_value_pairs_array["\"questionType\""] = "\"ValueQuestion\"";
        key_value_pairs_array["\"valueType\""] = "\"NumberQuestionValueType\"";
        return 0;
    }
    else if (enum == question_value_date)
    {
        key_value_pairs_array["\"questionType\""] = "\"ValueQuestion\"";
        key_value_pairs_array["\"valueType\""] = "\"DateQuestionValueType\"";
        return 0;
    }
    else if (enum == question_value_text)
    {
        key_value_pairs_array["\"questionType\""] = "\"ValueQuestion\"";
        key_value_pairs_array["\"valueType\""] = "\"TextQuestionValueType\"";
        return 0;
    }
    else if (enum == question_integration)
    {
        key_value_pairs_array["\"questionType\""] = "\"IntegrationQuestion\"";
        # not complete: more to add re schema

        return 0;
    }

    return 1;
}

function gen_uuid(     _uuid)
{
    if (("uuidgen" | getline _uuid) > 0)
    {
        uuid = _uuid;
    }
    else
    {
        uuid = "00000000-0000-0000-0000-000000000000";
    }
    close("uuidgen");
    
    return uuid;
}

function matches_uuid(str)
{
    if (str ~ /^[A-F0-9a-f]{8}-[A-F0-9a-f]{4}-[A-F0-9a-f]{4}-[A-F0-9a-f]{4}-[A-F0-9a-f]{12}$/)
    {
        return 1;
    }
    return 0;
}

function uuid_is_unique(uuid, uuid_set)
# this may seem a strange thing to do
# but, some uuids are obtained from cut and paste into a csv file
# so, who knows, we might well have duplicates....
#
# hence the need for an array uuid_set because one can check
# for key membership! (not value, recall)
{
    if (uuid in uuid_set)
    {
        return 0;
    }
    return 1;
}
    
function write_starting_brace(output_files)
{
    printf("\n{\n") >> output_files["json_file"];
}
function write_starting_bracket(output_files)
{
    printf("\n[\n") >> output_files["json_file"];
}
function write_ending_brace(output_files)
{
    printf("\n}") >> output_files["json_file"];
}
function write_ending_bracket(output_files)
{
    printf("\n]") >> output_files["json_file"];
}


function write_km_starting_tag(output_files)
{
    write_starting_brace(output_files);
}
function write_km_ending_tag(output_files)
{
    return write_ending_brace(output_files);
}


function write_km_globals(organizationId, name, version, kmId, 
                          metamodelVersion, 
                          output_files,     id)
{
    printf("\"organizationId\": \"%s\",\n", organizationId) \
        >> output_files["json_file"];
    printf("\"version\": \"%s\",\n", version) >> output_files["json_file"];

    id = organizationId ":" kmId ":" version;
    printf("\"id\": \"%s\",\n", id) >> output_files["json_file"];
    
    printf("\"name\": \"%s\",\n", name) >> output_files["json_file"];
    printf("\"kmId\": \"%s\",\n", kmId) >> output_files["json_file"];
    printf("\"metamodelVersion\": %d,\n", metamodelVersion) \
        >> output_files["json_file"];
}

# here we are talking about the array/bundle of packages
# A package is a a single KM package that holds changes
# (or more precisely events) for the specific version.
function write_packages_starting_tag(output_files)
{
    printf("\"packages\": \n[") >> output_files["json_file"];
}
function write_packages_ending_tag(output_files)
{
    write_ending_bracket(output_files);
}

# and here, we are talking about an individual package
function write_package_starting_tag(output_files)
{
    write_starting_brace(output_files);
}
function write_package_ending_tag(output_files)
{
    write_ending_brace(output_files);
}

function write_package_globals(organizationId, name, version, kmId, 
                               metamodelVersion,
                               license, description,
                               vprevious_version,
                               mergeCheckpointPackageId, 
                               forkOfPackageId, readme, 
                               output_files,
                               _id,
                               _previous_id,
                               _createdAt)

{
    printf("\"organizationId\": \"%s\",\n", organizationId)     \
        >> output_files["json_file"];
    printf("\"version\": \"%s\",\n", version) >> output_files["json_file"];
    
    _id = organizationId ":" kmId ":" version;
    printf("\"id\": \"%s\",\n", _id) >> output_files["json_file"];

    printf("\"license\": \"%s\",\n", license) >> output_files["json_file"];
    
    printf("\"name\": \"%s\",\n", name) >> output_files["json_file"];
    printf("\"kmId\": \"%s\",\n", kmId) >> output_files["json_file"];
    printf("\"metamodelVersion\": %d,\n", metamodelVersion) \
        >> output_files["json_file"];

    _createdAt = get_iso8601_date_time();
    printf("\"createdAt\": \"%s\",\n", _createdAt) \
        >> output_files["json_file"];

    printf("\"description\": \"%s\",\n", description)   \
        >> output_files["json_file"];
    printf("\"readme\": \"%s\",\n", readme)   \
        >> output_files["json_file"];

    if (vprevious_version == "none")
    {
        printf("\"previousPackageId\": null,\n") >> output_files["json_file"];
    }
    else
    {
        _previous_id = organizationId ":" kmId ":" vprevious_version;
        printf("\"previousPackageId\": \"%s\",\n", _previous_id) \
            >> output_files["json_file"];
    }
    if (mergeCheckpointPackageId == "null")
    {
        printf("\"mergeCheckpointPackageId\": null,\n") \
            >> output_files["json_file"];
    }
    else
    {
        printf("\"mergeCheckpointPackageId\": \"%s\",\n", \
               mergeCheckpointPackageId)        \
            >> output_files["json_file"];
    }
    if (forkOfPackageId == "null")
    {
        printf("\"forkOfPackageId\": null,\n") >> output_files["json_file"];
    }
    else
    {
        printf("\"forkOfPackageId\": \"%s\",\n", forkOfPackageId) \
            >> output_files["json_file"];
    }
}

function write_events_starting_tag(output_files)
{
    printf("\"events\": \n[") >> output_files["json_file"];
}
function write_events_ending_tag(output_files)
{
    write_ending_bracket(output_files);
}

function prepare_km_event(km_uuid, km_name, km_level,
                          uuid_levels, uuid_stack, last_uuid,
                          this_version_events_order,
                          this_version_events,
                          _uuid)
{
    assert(this_version_events_order[0] == 0,
           "this_version_events_order[0] == 0");
    # km event is truly the first event in the event list
    
    this_version_events_order[++this_version_events_order[0]] = km_uuid;
    this_version_events[km_uuid]["\"objectType\""] = "\"KnowledgeModel\"";

    ##############   uuids

    this_version_events[km_uuid]["\"parentUuid\""] \
        = "\"00000000-0000-0000-0000-000000000000\"";
    this_version_events[km_uuid]["\"entityUuid\""] = "\""km_uuid"\"";

    uuid_levels[km_uuid] = km_level;
    uuid_stack[++last_uuid] = km_uuid;
    # this uuid needs to be saved to construct the parent-child relationships
    
    _uuid = gen_uuid();
    this_version_events[km_uuid]["\"uuid\""] = "\""_uuid"\"";

    ###############  remainder of the object

    this_version_events[km_uuid]["\"name\""] = "\""km_name"\"";
    
    return last_uuid;
}

function write_comma(output_file)
{
    printf("\n,") >> output_file;
}

function prepare_phase_event(km_level,
                             level, label, uuid,
                             uuid_levels,
                             uuid_stack, last_uuid,
                             this_version_events_order,
                             this_version_events,
                             _parent_uuid, _uuid)
{
    this_version_events_order[++this_version_events_order[0]] = uuid;
    this_version_events[uuid]["\"objectType\""] = "\"Phase\"";

    ##############   uuids
    
    # the parent_uuid will be the uuid of the first item
    # off the stack whose level is == level - 1;
    # (level is the phase's)
    #

    while (uuid_levels[uuid_stack[last_uuid]] != level - 1)
    {
        last_uuid--;
    }
    
    _parent_uuid = uuid_stack[last_uuid];

    # it must the case that the parent of a phase is the km itself
    assert(uuid_levels[_parent_uuid] == km_level,
           "uuid_levels[_parent_uuid] == km_level");
    
    this_version_events[uuid]["\"parentUuid\""] = "\""_parent_uuid"\"";
    this_version_events[uuid]["\"entityUuid\""] = "\""uuid"\"";

    uuid_stack[++last_uuid] = uuid;
    # this uuid needs to be saved to construct the parent-child relationships
    uuid_levels[uuid_stack[last_uuid]] = level;
    
    _uuid = gen_uuid();

    this_version_events[uuid]["\"uuid\""] = "\""_uuid"\"";
    # this uuid is the event's one and needs not be saved

    ###############  remainder of the object

    this_version_events[uuid]["\"title\""] = "\""label"\"";
    this_version_events[uuid]["\"description\""] = "\"\"";

    return last_uuid;
}

function prepare_metric_event(km_level,
                              level,
                              title, description, abbreviation,
                              uuid,
                              uuid_levels,
                              uuid_stack,
                              last_uuid,
                              this_version_events_order,
                              this_version_events,
                              _parent_uuid, _uuid)
{
    this_version_events_order[++this_version_events_order[0]] = uuid;
    this_version_events[uuid]["\"objectType\""] = "\"Metric\"";

    ##############   uuids
    
    # the parent_uuid will be the uuid of the first item
    # off the stack whose level is == level - 1;
    # (level is the phase's)
    #

    while (uuid_levels[uuid_stack[last_uuid]] != level - 1)
    {
        last_uuid--;
    }
    
    _parent_uuid = uuid_stack[last_uuid];

    # it must the case that the parent of a phase is the km itself
    assert(uuid_levels[_parent_uuid] == km_level,
           "uuid_levels[_parent_uuid] == km_level");
    
    this_version_events[uuid]["\"parentUuid\""] = "\""_parent_uuid"\"";
    this_version_events[uuid]["\"entityUuid\""] = "\""uuid"\"";

    uuid_stack[++last_uuid] = uuid;
    # this uuid needs to be saved to construct the parent-child relationships
    uuid_levels[uuid_stack[last_uuid]] = level;
    
    _uuid = gen_uuid();

    this_version_events[uuid]["\"uuid\""] = "\""_uuid"\"";
    # this uuid is the event's one and needs not be saved

    ###############  remainder of the object

    this_version_events[uuid]["\"title\""] = "\""title"\"";
    this_version_events[uuid]["\"description\""] = "\""description"\"";
    this_version_events[uuid]["\"abbreviation\""] = "\""abbreviation"\"";

    return last_uuid;
}

function prepare_chapter_event(km_level,
                               level, label, text, uuid,
                               text_file_to_read,
                               uuid_levels,
                               uuid_stack, last_uuid,
                               this_version_events_order,
                               this_version_events,
                               _read_from_file,
                               _parent_uuid, _uuid)
{
    this_version_events_order[++this_version_events_order[0]] = uuid;
    this_version_events[uuid]["\"objectType\""] = "\"Chapter\"";

    ##############   uuids
    
    #print last_uuid, level;
    #print uuid_levels[uuid_stack[last_uuid]];
    #print uuid_stack[last_uuid];
    # the parent_uuid will be the uuid of the first item
    # off the stack whose level is == level - 1;
    # (level is the chapter's)
    #
    # uuids off the stack whose level != level -1
    # are simply discarded
    # this btw assumes that there is an order to the rows of the
    # csv template file: an order that exactly reflects
    # the template chapter/questions hierarchy!

    while (uuid_levels[uuid_stack[last_uuid]] != level - 1)
    {
        last_uuid--;
    }
    
    _parent_uuid = uuid_stack[last_uuid];

    # it must the case that the parent of a chapter is the km itself
    assert(uuid_levels[_parent_uuid] == km_level,
           "uuid_levels[_parent_uuid] == km_level");
    
    this_version_events[uuid]["\"parentUuid\""] = "\""_parent_uuid"\"";
    this_version_events[uuid]["\"entityUuid\""] = "\""uuid"\"";

    uuid_stack[++last_uuid] = uuid;
    # this uuid needs to be saved to construct the parent-child relationships
    uuid_levels[uuid_stack[last_uuid]] = level;
    
    _uuid = gen_uuid();

    this_version_events[uuid]["\"uuid\""] = "\""_uuid"\"";
    # this uuid is the event's one and needs not be saved

    ###############  remainder of the object

    this_version_events[uuid]["\"title\""] = "\""label"\"";
    
    # we must check if there is a text file to read as well
    # if this is the case then the content of the file
    # will be appended to the text parameter
    # 
    # it doesn't matter if there is no file to read:
    # handled by get_txt_file_content
    _read_from_file = get_txt_file_content(text_file_to_read);
    
    # json doesn't like multilines, so we trick it by using \n
    # which becomes \\n for awk escaping 
    text = text "\\n" _read_from_file;
    
    this_version_events[uuid]["\"text\""] = "\""text"\"";

    return last_uuid;
}

function  prepare_question_event(level, 
                                 array_type_label,
                                 uuid_levels,
                                 uuid_stack, uuid_set,
                                 last_uuid,
                                 phase_uuids,
                                 this_version_events_order,
                                 this_version_events,
                                 _parent_uuid,
                                 _entity_uuid,
                                 _uuid,
                                 _nb_children_so_far)
{
#    print "level " level;

    # _level might be -2: the case of a blank line or anything that is not
    # a question or answer (that is, that could be a chapter, file)
    if (level < 1)
    {
        return last_uuid;
    }

    ##############   uuids
    
    # the parent_uuid will be the uuid of the first item
    # off the stack whose level is == level - 1;
    # (level is the chapter's)
    #
    # uuids off the stack whose level != level -1
    # are simply discarded
    # this btw assumes that there is an order to the rows of the
    # csv template file: an order that exactly reflects
    # the template chapter/questions hierarchy!

    #_store_last_uuid = last_uuid;
    while (uuid_levels[uuid_stack[last_uuid]] != level - 1)
    {
        last_uuid--;

 #       if (last_uuid == 0)
  #      {
   #         print "assertion";
    #        print "level", level;
     #       print "last_uuid", _store_last_uuid, uuid_stack[_store_last_uuid];
      #      print "uuid", array_type_label["uuid"];
       #     print "label", array_type_label["question_label"];
       # }
        assert(last_uuid >= 0, "last_uuid >= 0");
    }

    _entity_uuid = array_type_label["uuid"];
    if (!uuid_is_unique(_entity_uuid, uuid_set))
    {
        print "faulty uuid " _entity_uuid;
    }
    assert(uuid_is_unique(_entity_uuid, uuid_set),
           "uuid_is_unique(_entity_uuid, uuid_set)");
    # include this uuid in the set
    _uuid_set[_entity_uuid] = 0;

    this_version_events_order[++this_version_events_order[0]]   \
        = _entity_uuid;
    this_version_events[_entity_uuid]["\"entityUuid\""] \
        = "\""_entity_uuid"\"";
    
    _parent_uuid = uuid_stack[last_uuid];

    this_version_events[_entity_uuid]["\"parentUuid\""] \
        = "\""_parent_uuid"\"";
    
    uuid_stack[++last_uuid] = _entity_uuid;
    # this uuid needs to be saved to construct the parent-child relationships
    uuid_levels[uuid_stack[last_uuid]] = level;
    
    _uuid = gen_uuid();

    this_version_events[_entity_uuid]["\"uuid\""] = "\""_uuid"\"";
    # this uuid is the event's one and needs not be saved

    ###############  remainder of the object

    #print "in prepare_write_create_question_event :";
    #print "  " array_type_label["question_type"];
    #print_enum(array_type_label["question_type"]);
    #print "  " array_type_label["question_label"];
    #print "  " array_type_label["question_text"];
    #print "  " array_type_label["answer_advice"];
    #print "   level " level;
    #print "   last_uuid " uuid_levels[uuid_stack[last_uuid]];

    if (array_type_label["question_type"] == option_answer)
    {
        this_version_events[_entity_uuid]["\"objectType\""] = "\"Answer\"";

        this_version_events[_entity_uuid]["\"label\""] \
            = "\""array_type_label["question_label"]"\"";
        this_version_events[_entity_uuid]["\"advice\""] \
            = "\""array_type_label["answer_advice"]"\"";
        # for the time being I am creating an empty array
        # must be modified --perhaps-- when we actually deal with them
        this_version_events[_entity_uuid]["\"metricMeasures\""][0] = 0;
    }
    else if (array_type_label["question_type"] == choice_answer)
    {
        this_version_events[_entity_uuid]["\"objectType\""] = "\"Choice\"";
        this_version_events[_entity_uuid]["\"label\""] \
            = "\""array_type_label["question_label"]"\"";
    }
    else
    {
        this_version_events[_entity_uuid]["\"objectType\""] = "\"Question\"";
        this_version_events[_entity_uuid]["\"title\""] \
            = "\""array_type_label["question_label"]"\"";

        this_version_events[_entity_uuid]["\"requiredPhaseUuid\""]      \
        = phase_uuids[3];

        assert(array_type_label["question_type"] != null,
               "array_type_label[question_type] != null");
        # the absence of this field makes dsw complain:
        # this field, like many others no doubt, are compulsory
        
        prepare_question_type_value(array_type_label["question_type"],
                                    this_version_events[_entity_uuid]);

        this_version_events[_entity_uuid]["\"text\""] \
            = "\""array_type_label["question_text"]"\"";
        
        #so for now, I decide to put requiredPhaseUuid to 3 (formerly 3, now
        # via a uuid)
        #and to refine this setting later on:
        # it will mean simply another column in the table
        # some other release

        this_version_events[_entity_uuid]["\"requiredPhaseUuid\""] \
            = phase_uuids[3];
        # this must be handled as well, later

        this_version_events[_entity_uuid]["\"tagUuids\""][0] = 0;
        # find out what tagUuids means....
        #

        if (array_type_label["question_type"] == question_list_of_items)
            # we must create an empty array for the field "\"itemTemplateQuestionUuids\""
        {
            this_version_events[_entity_uuid]["\"itemTemplateQuestionUuids\""][0] = 0;
        }
        
        # we are not done yet
        # for item list question, I need to gather its children into "itemTemplateQuestionUuids"
        # and in ORDER
        # it will be very dirty for now; don't know yet how to go about it
        #
        # note that we find this relationship via _parent_uuid
        assert(_parent_uuid in this_version_events, "_parent_uuid in this_version_events");
        if (this_version_events[_parent_uuid]["\"objectType\""] == "\"Question\"" \
            && this_version_events[_parent_uuid]["\"questionType\""] == "\"ListQuestion\"")

        {
            assert("\"itemTemplateQuestionUuids\"" in this_version_events[_parent_uuid], \
                   "itemTemplateQuestionUuids in this_version_events[_parent_uuid]");
            
            # we add the current _entity_uuid in the list of the children of _parent_uuid in ORDER
            assert(isarray(this_version_events[_parent_uuid]["\"itemTemplateQuestionUuids\""]), \
                   "(isarray(this_version_events[_parent_uuid][itemTemplateQuestionUuids])");
            _nb_children_so_far = this_version_events[_parent_uuid]["\"itemTemplateQuestionUuids\""][0];
            this_version_events[_parent_uuid]["\"itemTemplateQuestionUuids\""][++_nb_children_so_far] \
                = get_quotes_around(_entity_uuid);
            this_version_events[_parent_uuid]["\"itemTemplateQuestionUuids\""][0] = _nb_children_so_far;
        }
    }

    return last_uuid;
}

function process_events(header_array, table, max_row,
                        km_uuid, km_name,
                        vnchapters, max_level,
                        this_version_events_order,
                        this_version_events,
                        _km_level,
                        _uuid_set, _uuid_stack, _uuid_levels,
                        _last_uuid,
                        _row,
                        _n_phase, _phase_uuids,
                        _title, _abbreviation, _description,
                        _metric_array,
                        _n_chapters, 
                        _text_file_to_read, _question_file_to_read,
                        _filename, _ext,
                        _level, _label, _text, _uuid, _array_type_label,
                        _previous_relative_level, _relative_level,
                        _actual_line_level, _line)
{
    # we need to capture, for each uuid created, its level,
    # or depth of the question tree,
    # to manage the parent-child uuid relationship
    #
    # for uuid a to be a parent to uuid b, it must be the case
    # that level(uuid b) = level(uuid a) + 1
    #
    # both _uuid_stack and _uuid_levels stacks are used in
    # conjonction to allow for that
    split("", _uuid_stack);
    split("", _uuid_levels);

    _km_level = -1;
    # this is the level of the km, technically it should be 0
    # but for convenience, we want chapter to be at level 0
    # and everything else given by the column header "Level x"
    # and, again, for convenience, we start at 1 for x
    #
    # anyhow, what is important is not where we start, but that
    # succeeding chapter/question/option/choice are exactly one level
    # apart
    
    _last_uuid = 0;
    
    # this set is to allow for uniqueness check for uuids,
    # in case those haven't been generated properly (eg a stray copy & paste)
    split("", _uuid_set);
    _uuid_set[km_uuid] = 0;

    _last_uuid = prepare_km_event(km_uuid, km_name,
                                  _km_level,
                                  _uuid_levels,
                                  _uuid_stack, 
                                  _last_uuid,
                                  this_version_events_order,
                                  this_version_events);
    
    _n_phases = 0;
    split("", _phase_uuids);
    split("", _metric_array);
    
    _n_chapters = 0;
    for (_row = 2; _row <= max_row; _row++)
        # remember, the template is organised so that there is one row per
        # filled column:
        # that is, is there is a filled cell for column "level X" then
        # all cells in columns "level Y" will be empty
        # except:
        #   the text column might be filled in all cases;
        #   likewise, when the cell is an answer,
        #     then the advice column might be filled
        #
        # there is a simple assumption: we start at level 0 (a chapter)
        # to then build a tree (rather a forest) of questions rooted at
        # chapters, themselves rooted at the km
    {
   #     print "in loop row " _row;
#
 #        txt = "";
  #       for (k = 1; k < nfields; k++)
   #      {
    #         txt = txt ", " table[_row, k];
     #    }
      #   print txt;

        # recall that chapter and question are exclusive
        # of each other
        # but NOT file: hence the checks are written the way they are
        
        # check if we have a file inclusion
        _text_file_to_read = "";
        _question_file_to_read = "";
        if (is_file(header_array, table, _row))
        {
            _filename = get_filename(header_array, table, _row);
            _ext = get_file_type(_filename);
            if (_ext == "txt" || _ext == "md")
            {
                _text_file_to_read = _filename;
            }
            else if (_ext == "csv")
            {
                _question_file_to_read = _filename;
            }
            # no other choice
        }
        if (is_phase(header_array, table, _row))
        {
            _n_phases++;

            _level = get_level(max_level, header_array, table, _row); 
            # ok, this is not strictly necessary because
            # phase is always level 0, but, for the sake of coherence
            # with the other elements
            
            _label = get_phase(header_array, table, _row);
            _uuid = get_uuid(header_array, table, _row);
            if (!uuid_is_unique(_uuid, _uuid_set) || _uuid == "")
            {
                print "faulty or missing uuid" _uuid;
            }
            assert(uuid_is_unique(_uuid, _uuid_set),
                   "uuid_is_unique(_uuid, _uuid_set)");
            # include this uuid in the set
            _uuid_set[_uuid] = 0;

            _phase_uuids[0] = _n_phases;
            _phase_uuids[_n_phases] = get_quotes_around(_uuid);

            #print label, text;
            _last_uuid = prepare_phase_event(_km_level,
                                             _level, _label, _uuid,
                                             _uuid_levels,
                                             _uuid_stack,
                                             _last_uuid,
                                             this_version_events_order,
                                             this_version_events);
        }
        if (is_metric(header_array, table, _row))
        {
            _n_metrics++;

            _level = get_level(max_level, header_array, table, _row); 
            # ok, this is not strictly necessary because
            # metric is always level 0, but, for the sake of coherence
            # with the other elements

            _uuid = get_uuid(header_array, table, _row);
            if (!uuid_is_unique(_uuid, _uuid_set) || _uuid == "")
            {
                print "faulty or missing uuid" _uuid;
            }
            assert(uuid_is_unique(_uuid, _uuid_set),
                   "uuid_is_unique(_uuid, _uuid_set)");
            # include this uuid in the set
            _uuid_set[_uuid] = 0;

            _description = get_metric_description(header_array, table, _row);
            _title = get_metric(header_array, table, _row);
            _abbreviation = get_metric_abbreviation(header_array, table, _row);

            _metric_array[_uuid]["title"] = _title;
            _metric_array[_uuid]["abbrevation"] = _abbreviation;
            _metric_array[_uuid]["description"] = _description;

            _last_uuid = prepare_metric_event(_km_level,
                                              _level,
                                              _title, _description, _abbreviation,
                                              _uuid,
                                              _uuid_levels,
                                              _uuid_stack,
                                              _last_uuid,
                                              this_version_events_order,
                                              this_version_events);
        }
        if (is_chapter(header_array, table, _row))
        {
            _n_chapters++;

            if (_n_chapters > vnchapters)
                # break out of the loop altogether
            {
                break;
            }
            
            _level = get_level(max_level, header_array, table, _row);
            # ok, this is not strictly necessary because
            # chapter is always level 0, but, for the sake of coherence
            # with the other elements
            
            _label = get_chapter(header_array, table, _row);
            _text = get_text(header_array, table, _row);
            _uuid = get_uuid(header_array, table, _row);
            if (!uuid_is_unique(_uuid, _uuid_set) || _uuid == "")
            {
                print "faulty or missing uuid" _uuid;
            }
            assert(uuid_is_unique(_uuid, _uuid_set),
                   "uuid_is_unique(_uuid, _uuid_set)");
            # include this uuid in the set
            _uuid_set[_uuid] = 0;

            #print label, text;
            _last_uuid = prepare_chapter_event(_km_level,
                                               _level, _label, _text, _uuid,
                                               _text_file_to_read,
                                               _uuid_levels,
                                               _uuid_stack,
                                               _last_uuid,
                                               this_version_events_order,
                                               this_version_events);
        }
        else if (_level = is_question(max_level, 
                                      header_array, table, _row))
        {
            # can't be chapter, blank line, ....
            assert(_level > 0 && _level <= max_level,
                   "_level > 0 && _level <= max_level");
            
            # and another temp array to collect the info
            split("", _array_type_label);
            get_question_type_and_label(_level,
                                        header_array, table,
                                        _row,
                                        _text_file_to_read,
                                        _array_type_label);
            
            _last_uuid = prepare_question_event(_level, 
                                                _array_type_label,
                                                _uuid_levels,
                                                _uuid_stack, _uuid_set,
                                                _last_uuid,
                                                _phase_uuids,
                                                this_version_events_order,
                                                this_version_events);
        }

        if (_question_file_to_read)
            # that is there are more questions to process, but those
            # are tofound in additional csv files
        {
            _previous_relative_level = -1; # before starting the file read
            while ((getline _line < _question_file_to_read) > 0)
            {
                if (_line == "") # skip blank lines
                {
                    continue;
                }
                #print "in loop " line;
                # the same temp array to collect the info
                split("", _array_type_label);

                _relative_level = get_question_type_and_label_from_file( \
                    _line,
                    _array_type_label);
                #print "in loop previous_relative_level " previous_relative_level;
                #print "in loop relative_level " relative_level;
                #print "in loop level " level;
                assert(_relative_level >= 1, "_relative_level >= 1");

                if (_previous_relative_level >= 0)
                    # ie this is NOT the first line of the file we read
                {
                    if (_relative_level > _previous_relative_level)
                    {
                        assert(_relative_level == _previous_relative_level + 1,
                               "_relative_level == _previous_relative_level + 1");
                    }
                }

                _actual_line_level = _level + _relative_level;
                _last_uuid = prepare_question_event(_actual_line_level, 
                                                    _array_type_label,
                                                    _uuid_levels,
                                                    _uuid_stack, _uuid_set, 
                                                    _last_uuid,
                                                    _phase_uuids,
                                                    this_version_events_order,
                                                    this_version_events);
                
                _previous_relative_level = _relative_level;
            }
        }
    }
}


function object_to_create_action(object)
{
    switch (object)
    {
        case "\"KnowledgeModel\"":
        {
            return "\"AddKnowledgeModelEvent\"";
        }
        case "\"Chapter\"":
        {
            return "\"AddChapterEvent\"";
        }
        case "\"Question\"":
        {
            return "\"AddQuestionEvent\"";
        }
        case "\"Answer\"":
        {
            return "\"AddAnswerEvent\"";
        }
        case "\"Choice\"":
        {
            return "\"AddChoiceEvent\""; 
        }
        case "\"Metric\"":
        {
            return "\"AddMetricEvent\""; 
        }
        case "\"Phase\"":
        {
            return "\"AddPhaseEvent\""; 
        }
        default:
        {
            return "";
        }
    }
}
function object_to_edit_action(object)
{
    switch (object)
    {
        case "\"KnowledgeModel\"":
        {
            return "\"EditKnowledgeModelEvent\"";
        }
        case "\"Chapter\"":
        {
            return "\"EditChapterEvent\"";
        }
        case "\"Question\"":
        {
            return "\"EditQuestionEvent\"";
        }
        case "\"Answer\"":
        {
            return "\"EditAnswerEvent\"";
        }
        case "\"Choice\"":
        {
            return "\"EditChoiceEvent\""; 
        }
        case "\"Metric\"":
        {
            return "\"EditMetricEvent\""; 
        }
        case "\"Phase\"":
        {
            return "\"EditPhaseEvent\""; 
        }
        default:
        {
            return "";
        }
    }
}
function object_to_move_action(object)
{
    switch (object)
    {
        case "\"Question\"":
        {
            return "\"MoveQuestionEvent\"";
        }
        case "\"Answer\"":
        {
            return "\"MoveAnswerEvent\"";
        }
        default:
        {
            return "";
        }
    }
}
function object_to_delete_action(object)
{
    switch (object)
    {
        case "\"Chapter\"":
        {
            return "\"DeleteChapterEvent\"";
        }
        case "\"Question\"":
        {
            return "\"DeleteQuestionEvent\"";
        }
        case "\"Answer\"":
        {
            return "\"DeleteAnswerEvent\"";
        }
        case "\"Choice\"":
        {
            return "\"DeleteChoiceEvent\""; 
        }
        case "\"Metric\"":
        {
            return "\"DeleteMetricEvent\""; 
        }
        case "\"Phase\"":
        {
            return "\"DeletePhaseEvent\""; 
        }
        default:
            # note: there is no delete event for a KM
        {
            return "";
        }
    }
}
function event_type_to_object_type(event_type)
{
    switch (event_type)
    {
        case "\"AddKnowledgeModelEvent\"":
        case "\"EditKnowledgeModelEvent\"":
        {
            return "\"KnowledgeModel\"";
        }
        case "\"AddChapterEvent\"":
        case "\"EditChapterEvent\"":
        {
            return "\"Chapter\"";
        }
        case "\"AddQuestionEvent\"":
        case "\"EditQuestionEvent\"":
        {
            return "\"Question\"";
        }
        case "\"AddAnswerEvent\"":
        case "\"EditAnswerEvent\"":
        {
            return "\"Answer\"";
        }
        case "\"AddChoiceEvent\"":
        case "\"EditChoiceEvent\"":
        {
            return "\"Choice\""; 
        }
        case "\"AddMetricEvent\"":
        case "\"EditMetricEvent\"":
        {
            return "\"Metric\""; 
        }
        case "\"AddPhaseEvent\"":
        case "\"EditPhaseEvent\"":
        {
            return "\"Phase\""; 
        }
        default:
        {
            return "";
        }
    }
}

function list_question_types(question_types)
{
    question_types["\"OptionsQuestion\""] = 0;
    question_types["\"ChoiceQuestion\""] = 0;
    question_types["\"ListQuestion\""] = 0;
    question_types["\"ValueQuestion\""] = 0;
    question_types["\"IntegrationQuestion\""] = 0;
}

function non_editable_fields(fields)
{
    fields["\"uuid\""] = "";
    fields["\"parentUuid\""] = "";
    fields["\"entityUuid\""] = "";
    # questionType is really special: will need to explain
    # this in more detail, later hopefully
    # (basically one can change questionType with
    # consequences I have to describe, but questionType
    # will not be marked as "changed" --this doesn't seem
    # consistent to me...)
    #fields["\"questionType\""] = "";

    # fields["\"parentUuid\""] is even more special since, in the
    # case of a move event, it is then modified...
}

function event_type_to_fields(event_type, question_type, fields)
{
    switch (event_type)
    {
        case "\"AddKnowledgeModelEvent\"":
        {
            fields["\"uuid\""] = "";
            fields["\"parentUuid\""] = "";
            fields["\"entityUuid\""] = "";
            fields["\"name\""] = "";
            break;
        }
        case "\"EditKnowledgeModelEvent\"":
        {
            fields["\"uuid\""] = "";
            fields["\"parentUuid\""] = "";
            fields["\"entityUuid\""] = "";
            fields["\"name\""] = "";
            fields["\"chapterUuids\""] = "";
            fields["\"tagUuids\""] = "";
            fields["\"integrationUuids\""] = "";
            break;
        }
        case "\"AddChapterEvent\"":
        {
            fields["\"uuid\""] = "";
            fields["\"parentUuid\""] = "";
            fields["\"entityUuid\""] = "";
            fields["\"title\""] = "";
            fields["\"text\""] = "";
            break;
        }
        case "\"EditChapterEvent\"":
        {
            fields["\"uuid\""] = "";
            fields["\"parentUuid\""] = "";
            fields["\"entityUuid\""] = "";
            fields["\"title\""] = "";
            fields["\"text\""] = "";
            fields["\"questionUuids\""] = "";
            break;
        }
        case "\"DeleteChapterEvent\"":
        {
            fields["\"uuid\""] = "";
            fields["\"parentUuid\""] = "";
            fields["\"entityUuid\""] = "";
            break;
        }
        case "\"AddQuestionEvent\"":
        {
            fields["\"uuid\""] = "";
            fields["\"parentUuid\""] = "";
            fields["\"entityUuid\""] = "";
            fields["\"title\""] = "";
            fields["\"text\""] = "";
            fields["\"tagUuids\""] = "";
            fields["\"requiredPhaseUuid\""] = "";
            switch (question_type)
            {
                case "\"OptionsQuestion\"":
                {
                    fields["\"questionType\""] = "\"OptionsQuestion\"";
                    break;
                }
                case "\"ChoiceQuestion\"":
                {
                    fields["\"questionType\""] = "\"ChoiceQuestion\"";
                    break;
                }
                case "\"ListQuestion\"":
                {
                    fields["\"questionType\""] = "\"ListQuestion\"";
                    break;
                }
                case "\"ValueQuestion\"":
                {
                    fields["\"questionType\""] = "\"ValueQuestion\"";
                    fields["\"valueType\""] = "";
                    break;
                }
                case "\"IntegrationQuestion\"":
                {
                    fields["\"questionType\""] = "\"IntegrationQuestion\"";
                    fields["\"integrationUuid\""] = "";
                    fields["\"props\""] = "";
                    break;
                }
            }
            break;
        }
        case "\"EditQuestionEvent\"":
        {
            fields["\"uuid\""] = "";
            fields["\"parentUuid\""] = "";
            fields["\"entityUuid\""] = "";
            fields["\"title\""] = "";
            fields["\"text\""] = "";
            fields["\"tagUuids\""] = ""; 
            fields["\"expertUuids\""] = "";
            fields["\"referenceUuids\""] = "";
            fields["\"requiredPhaseUuid\""] = "";
            switch (question_type)
            {
                case "\"OptionsQuestion\"":
                {
                    fields["\"questionType\""] = "\"OptionsQuestion\"";
                    fields["\"answerUuids\""] = "";
                    break;
                }
                case "\"MultiChoiceQuestion\"":
                {
                    fields["\"questionType\""] = "\"MultiChoiceQuestion\"";
                    fields["\"choiceUuids\""] = "";
                    break;
                }
                case "\"ListQuestion\"":
                {
                    fields["\"questionType\""] = "\"ListQuestion\"";
                    fields["\"itemTemplateQuestionUuids\""] = "";
                    break;
                }
                case "\"ValueQuestion\"":
                {
                    fields["\"questionType\""] = "\"ValueQuestion\"";
                    fields["\"valueType\""] = "";
                    break;
                }
                case "\"IntegrationQuestion\"":
                {
                    fields["\"questionType\""] = "\"IntegrationQuestion\"";
                    fields["\"integrationUuid\""] = "";
                    fields["\"props\""] = "";
                    break;
                }
            }
            break;
        }
        case "\"MoveQuestionEvent\"":
        {
            fields["\"uuid\""] = "";
            fields["\"parentUuid\""] = "";
            fields["\"entityUuid\""] = "";
            fields["\"targetUuid\""] = "";
            break;
        }
        case "\"DeleteQuestionEvent\"":
        {
            fields["\"uuid\""] = "";
            fields["\"parentUuid\""] = "";
            fields["\"entityUuid\""] = "";
            break;
        }
        case "\"AddAnswerEvent\"":
        {
            fields["\"uuid\""] = "";
            fields["\"parentUuid\""] = "";
            fields["\"entityUuid\""] = "";
            fields["\"label\""] = "";
            fields["\"advice\""] = "";
            fields["\"metricMeasures\""] = "";
            break;
        }
        case "\"EditAnswerEvent\"":
        {
            fields["\"uuid\""] = "";
            fields["\"parentUuid\""] = "";
            fields["\"entityUuid\""] = "";
            fields["\"label\""] = "";
            fields["\"advice\""] = "";
            fields["\"metricMeasures\""] = "";
            fields["\"followUpUuids\""] = "";
            break;
        }
        case "\"MoveAnswerEvent\"":
        {
            fields["\"uuid\""] = "";
            fields["\"parentUuid\""] = "";
            fields["\"entityUuid\""] = "";
            fields["\"targetUuid\""] = "";
            break;
        }
        case "\"DeleteAnswerEvent\"":
        {
            fields["\"uuid\""] = "";
            fields["\"parentUuid\""] = "";
            fields["\"entityUuid\""] = "";
            break;
        }
        case "\"AddChoiceEvent\"":
        {
            fields["\"uuid\""] = "";
            fields["\"parentUuid\""] = "";
            fields["\"entityUuid\""] = "";
            fields["\"label\""] = "";
            break;
        }
        case "\"EditChoiceEvent\"":
        {
            fields["\"uuid\""] = "";
            fields["\"parentUuid\""] = "";
            fields["\"entityUuid\""] = "";
            fields["\"label\""] = "";
            break;
        }
        case "\"DeleteChoiceEvent\"":
        {
            fields["\"uuid\""] = "";
            fields["\"parentUuid\""] = "";
            fields["\"entityUuid\""] = "";
            break;
        }
        case "\"AddMetricEvent\"":
        {
            fields["\"eventType\""] = "";
            fields["\"uuid\""] = "";
            fields["\"parentUuid\""] = "";
            fields["\"entityUuid\""] = "";
            fields["\"title\""] = "";
            fields["\"abbreviation\""] = "";
            fields["\"description\""] = "";
        }
        case "\"EditMetricEvent\"":
        {
            fields["\"eventType\""] = "";
            fields["\"uuid\""] = "";
            fields["\"parentUuid\""] = "";
            fields["\"entityUuid\""] = "";
            fields["\"title\""] = "";
            fields["\"abbreviation\""] = "";
            fields["\"description\""] = "";
        }
        case "\"DeleteMetricEvent\"":
        {
            fields["\"eventType\""] = "";
            fields["\"uuid\""] = "";
            fields["\"parentUuid\""] = "";
            fields["\"entityUuid\""] = "";
        }
        case "\"AddPhaseEvent\"":
        {
            fields["\"eventType\""] = "";
            fields["\"uuid\""] = "";
            fields["\"parentUuid\""] = "";
            fields["\"entityUuid\""] = "";
            fields["\"title\""] = "";
            fields["\"description\""] = "";
        }
        case "\"EditPhaseEvent\"":
        {
            fields["\"eventType\""] = "";
            fields["\"uuid\""] = "";
            fields["\"parentUuid\""] = "";
            fields["\"entityUuid\""] = "";
            fields["\"title\""] = "";
            fields["\"description\""] = "";
        }
        case "\"DeletePhaseEvent\"":
        {
            fields["\"eventType\""] = "";
            fields["\"uuid\""] = "";
            fields["\"parentUuid\""] = "";
            fields["\"entityUuid\""] = "";
        }
        
     # didn't --yet-- do expert, resourcePage,
        # reference (various), integration
    }
}

function classify_event_type(events_classes)
{
    events_classes["add"]["\"AddKnowledgeModelEvent\""] = 0;
    events_classes["add"]["\"AddKnowledgeModelEvent\""] = 0;
    events_classes["add"]["\"AddChapterEvent\""] = 0;
    events_classes["add"]["\"AddQuestionEvent\""] = 0;
    events_classes["add"]["\"AddAnswerEvent\""] = 0;
    events_classes["add"]["\"AddChoiceEvent\""] = 0;
    events_classes["add"]["\"AddPhaseEvent\""] = 0;
    events_classes["add"]["\"AddMetricEvent\""] = 0;

    events_classes["edit"]["\"EditKnowledgeModelEvent\""] = 0;
    events_classes["edit"]["\"EditChapterEvent\""] = 0;
    events_classes["edit"]["\"EditQuestionEvent\""] = 0;
    events_classes["edit"]["\"EditAnswerEvent\""] = 0;
    events_classes["edit"]["\"EditChoiceEvent\""] = 0;
    events_classes["add"]["\"EditPhaseEvent\""] = 0;
    events_classes["add"]["\"EditMetricEvent\""] = 0;

    events_classes["move"]["\"MoveQuestionEvent\""] = 0;
    events_classes["move"]["\"MoveAnswerEvent\""] = 0;

    events_classes["delete"]["\"DeleteChapterEvent\""] = 0;
    events_classes["delete"]["\"DeleteQuestionEvent\""] = 0;
    events_classes["delete"]["\"DeleteAnswerEvent\""] = 0;
    events_classes["delete"]["\"DeleteChoiceEvent\""] = 0;
    events_classes["add"]["\"DeletePhaseEvent\""] = 0;
    events_classes["add"]["\"DeleteMetricEvent\""] = 0;
}



function matches_opening_bracket(str)
{
    if (str ~ /^\[/)
    {
        return 1;
    }
    return 0;
}
function matches_ending_bracket(str)
{
    if (str ~ /\]$/)
    {
        return 1;
    }
    return 0;
}
function strip_opening_bracket(str)
{
    if (length(str) > 1 && substr(str, 1, 1) == "[")
    {
        str = substr(str, 2, length(str) - 1);
    }
    return str;
}
function strip_ending_bracket(str)
{
    if (length(str) > 1 && substr(str, length(str), 1) == "]")
    {
        str = substr(str, 1, length(str) - 1);
    }
    return str;
}
function is_empty_array(str)
{
    if (str ~ /^\[\]$/)
    {
        return 1;
    }
    return 0;
}

function read_array_from_history_file(i,
                                      nb_of_arrays_read,
                                      fields_already_read,
                                      array_read,
                                      _nb_of_fields_in_array,
                                      _array_start_index,
                                      _field,
                                      _stripped_field)
{
    assert(matches_opening_bracket($i), "matches_opening_bracket($i)");

    _nb_of_fields_in_array = 0; # I need this to order the elts in the array
    # recall that awk arrays are associative and thus not ordered

    if (is_empty_array($i))
    {
        array_read[0] = 0;
        fields_already_read[i] = nb_of_arrays_read;
        
        return _nb_of_fields_in_array + 1;
    }

    _array_start_index = i;
    _field = strip_opening_bracket($i);
    while (! matches_ending_bracket($i))
    {
        if (_array_start_index == i)
            # the array has only one elt
        {
            _stripped_field = strip_quotes_from_string(_field);
            array_read[++_nb_of_fields_in_array] = _field;
        }
        else
        {
            _stripped_field = strip_quotes_from_string($i);
            array_read[++_nb_of_fields_in_array] = $i;
        }

        # this field can only be a uuid
        # as far as I know the only arrays in dsw entities consist of uuids
        assert(matches_uuid(_stripped_field),
               "matches_uuid(_stripped_field)");
                
        fields_already_read[i] = nb_of_arrays_read;
        i++;
    }
    assert(matches_ending_bracket($i), "matches_ending_bracket($i)");
           
    if (_array_start_index == i)
        # the array has only one elt
    {
        _field = strip_ending_bracket(_field);
    }
    else
    {
        _field = strip_ending_bracket($i);
    }
            
    _stripped_field = strip_quotes_from_string(_field);
    # this field can only be a uuid
    assert(matches_uuid(_stripped_field),
           "matches_uuid(_stripped_field)");

    array_read[++_nb_of_fields_in_array] \
        = _field;
    fields_already_read[i] = nb_of_arrays_read;

    # we have finished reading the array
    array_read[0] = _nb_of_fields_in_array;

    return _nb_of_fields_in_array;
}


function assert_array_in_uuid_tree(array_read,
                                   uuid_tree)
{
}

function deep_array_copy_to(array_a, array_b, _i)
{
    for (_i = 0; _i <= array_a[0]; _i++)
    {
        array_b[_i] = array_a[_i];
    }
}


function process_events_from_history_file_pass_1(arrays_read,
                                                 fields_already_read,
                                                 _i, _j, 
                                                 _temp_array,
                                                 _array_read,
                                                 _nb_of_arrays_read,
                                                 _nb_of_array_fields_read)
# global args not declared in the profile are: $0, NF
#
# in awk all vars are global, except when we make variables local
# to functions
#
# typically I declare all global vars used in the function in the profile
# for legibility
#
# this function reads all the arrays in $0
{
    _nb_of_arrays_read = 0;
    for (_i = 1; _i <= NF; _i++)
    {
        # we must 'weed' out any events where we might have to read arrays
        if (matches_opening_bracket($_i))
        {
            _nb_of_arrays_read++;
            
            split ("", _array_read);
            _nb_of_array_fields_read = read_array_from_history_file(_i,
                                                                    _nb_of_arrays_read,
                                                                    fields_already_read,
                                                                    _array_read); 
 
    #  deep_array_copy_to(_array_read, _arrays_read[_nb_of_arrays_read]["array"])
     #               
            for (_j = 0; _j <= _array_read[0]; _j++)
            {
                arrays_read[_nb_of_arrays_read]["array"][_j] = _array_read[_j];
            }
            arrays_read[_nb_of_arrays_read]["nb_fields_read"] \
                = _nb_of_array_fields_read;

            _i = _i + _nb_of_array_fields_read - 1;
            assert(_i <= NF, "_i <= NF");
        }
    }
}

function process_events_from_history_file_pass_2(arrays_read,
                                                 fields_already_read,
                                                 field_read_not_in_array,
                                                 temp_array,
                                                 _i, _j, _k,
                                                 _which_array,
                                                 _nb_of_fields_in_array,
                                                 _field, 
                                                 _value, _value_array,
                                                 _changed)
# global args not declared in the profile are: $0, NF
#
# in awk all vars are global, except when we make variables local
# to functions
#
# typically I declare all global vars used in the function in the profile
# for legibility
#
# this function reads edit events where some fields (as transcribed in the csv file)
# are followed by the { "changed":, "value":} block
{
    for (_i = 1; _i <= NF; _i++)
    {
        if ($_i == "\"changed\"")
            # this is the most involved of the cases since the way the csv-like
            # layout/parsing of the json package will read as, for example,
            #
            # "tagUuids","changed",false
            # or
            # "text","value","blah","changed",true
            #
            # that is:
            #    field_label, value, new_value, changed, true
            #    (or field_label, changed, true, value, new_value)
            # or
            #    field_label, changed, false
            #
            # on top of this, new_value may be an array!
            #
            # arrays may be read for add events:
            # tagUuids (addQuestion)
            # metricMeasures (addAnswer)
            # props (addIntegration)
            # otherwise all arrays are for edit events
            #
            # since I don't yet deal with these fields,
            # we can assume FOR NOW that each time we are reading an array,
            # it is associated with an edit event
        {
            split("", _value_array);
            assert(!(0 in _value_array), "!(0 in _value_array)");
            assert(_i < NF, "_i < NF");
            if ($(_i + 1) == "false")
                # case field_label, changed, false
            {
                assert(_i > 1, "_i > 1");
                _field = $(_i - 1);
                _changed = "false";

                # in this case, we know for certain that 
                # fields_already_read from _i-1 to _i+1 have not been
                # read into an array before (1rst pass)
                assert(fields_already_read[_i - 1] == 0 \
                       && fields_already_read[_i] == 0 \
                       && fields_already_read[_i + 1] == 0, \
                       "fields_already_read[_i - 1] == 0 \
                       && fields_already_read[_i] == 0 \
                       && fields_already_read[_i + 1] == 0");
                
                fields_already_read[_i - 1] = field_read_not_in_array;
                fields_already_read[_i] = field_read_not_in_array;
                fields_already_read[_i + 1] = field_read_not_in_array;

                temp_array[_field]["changed"] = 0;
                i++;
            }
            else
                # there are two cases to handle:
                # when the field block is read as
                # "field" : { "value": xxxx, "changed": true } --1rst case
                # or "field" : { "changed": true, "value": xxxx } 2nd case
                #
                # before being parsed as csv, that is, as already shown above
                # 
                #   field_label, value, new_value, changed, true
                # or
                #   field_label, changed, true, value, new_value
                #
                # note: the json parsing as csv-like is not done
                # by this awk script, but by the corresponding sh script 
            {
                assert($(_i + 1) == "true", "$(_i + 1) == \"true\"");
                if (fields_already_read[_i - 1] != 0)
                    # this implies that the new_value field is an array,
                    # and, it can only be the corresponding new_value field
                    # the only other choice for _i-1 is a field_value
                    # which has been read yet
                    # so fields_already_read[_i - 1] is necessarily 0
                    #
                    # so we are in the
                    # case field_label, value, new_value, changed, true
                    # new_value is an array
                {
                    _which_array = fields_already_read[_i - 1];
                    _j = _i - 1;
                    while (fields_already_read[_j] == _which_array)
                    {
                        _j--;
                    }
                    assert(_j > 1, "_j > 1");
                    assert(fields_already_read[_j] == 0,\
                           "fields_already_read[_j] == 0");
                    assert($_j == "\"value\"", "$_j == value");
                    
                    assert(_j - 1 > 1, "_j - 1 > 1");
                    _field = $(_j - 1);
                    _changed = "true";

                    deep_array_copy_to(arrays_read[_which_array]["array"], _value_array);
                    
                    fields_already_read[_j - 1] = field_read_not_in_array;
                    fields_already_read[_j] = field_read_not_in_array;

                    _j++;
                    for (_j; _j < _i; _j++)
                        # overwrite  fields_already_read
                        # to indicate that this array has been dealt with
                    {
                        fields_already_read[_j] = field_read_not_in_array;
                    }
                    
                    fields_already_read[_i] = field_read_not_in_array;
                    fields_already_read[_i + 1] = field_read_not_in_array;
                    _i++;
                }
                else if (_i - 2 > 1 && $(_i - 2) == "\"value\"")
                    # case field_label, value, new_value, changed, true
                    # new_value is NOT an array
                {
                    assert(_i - 2 > 1, "_i - 2 > 1");
                    _field = $(_i - 3);
                    _changed = "true";
                    _value = $(_i - 1); 
                    fields_already_read[_i - 3] = field_read_not_in_array;
                    fields_already_read[_i - 2] = field_read_not_in_array;
                    fields_already_read[_i - 1] = field_read_not_in_array;
                    fields_already_read[_i] = field_read_not_in_array;
                    fields_already_read[_i + 1] = field_read_not_in_array;
                    _i++;
                }
                else # case field_label, changed, true, value, new_value
                {
                    assert(_i + 3 <= NF, "_i + 3 <= NF");
                    assert($(_i + 2) == "\"value\"",
                           "$(_i + 2) == \"\"value\"\");");
                    
                    if (fields_already_read[_i + 3] != 0)
                        #  new_value is an array
                    {
                        _which_array = fields_already_read[_i + 3];
                        deep_array_copy_to(arrays_read[_which_array]["array"], _value_array);
                    }
                    else
                        #  new_value is NOT an array
                    {
                        _value = $(_i + 3);
                    }
             
                    assert(_i > 1, "_i > 1");
                    assert(fields_already_read[_i - 1] == 0,\
                           "fields_already_read[_i -1] == 0");
                    _field = $(_i - 1);
                    _changed = "true";

                    fields_already_read[_i - 1] = field_read_not_in_array;
                    fields_already_read[_i] = field_read_not_in_array;
                    fields_already_read[_i + 1] = field_read_not_in_array;
                    fields_already_read[_i + 2] = field_read_not_in_array;
                    if (fields_already_read[_i + 3] == 0)
                    {
                        fields_already_read[_i + 3] = field_read_not_in_array;
                        _i += 3;
                    }
                    else
                    {
                        _nb_of_fields_in_array \
                            = arrays_read[_which_array]["nb_fields_read"];

                        # overwrite  fields_already_read
                        # to indicate that this array has been dealt with
                        _i += 3;
                        for (_i; _i < _i + _nb_of_fields_in_array; _i++)
                        {
                            fields_already_read[_i] = field_read_not_in_array;
                        }
                    }
                }

                temp_array[_field]["changed"] = 1;
                if (0 in _value_array)
                    # this array has been read into
                {
                    delete temp_array[_field]["value"];
                    temp_array[_field]["value"][0] = 0;
                    deep_array_copy_to(_value_array, temp_array[_field]["value"]);
                }
                else
                {
                    temp_array[_field]["value"] = _value;
                }
            }
        }
    }
}

function process_events_from_history_file_pass_3(arrays_read,
                                                 fields_already_read,
                                                 field_read_not_in_array,
                                                 temp_array,
                                                 _i, _j,
                                                 _which_array,
                                                 _nb_of_fields_in_array)
# global args not declared in the profile are: $0, NF
#
# in awk all vars are global, except when we make variables local
# to functions
#
# typically I declare all global vars used in the function in the profile
# for legibility
#
# this function reads anything that has been left unread by pass 1 and 2
# and constructs the temp array temp_array holding every field
# from the input line $0
{
    # 3rd pass
    # now read the remaining fields:
    # those are whose fields_already_read[_i] == 0 (never read)
    # or fields_already_read[_i] == some index referring to an array number
    # an array that hasn't yet been dealt with in pass 2

#       for (_i = 1; _i <= NF; _i++)
#        {
#            print "before 3rd pass", _i, $_i, fields_already_read[_i];
#        }
#
#       for (_field in temp_array)
#        {
#            if (!(isarray(temp_array[_field])))
#               print _field, temp_array[_field];
#        }
    for (_i = 1; _i <= NF; _i++)
    {
        if (! fields_already_read[_i])
            # so we read the key-value pairs at _i and _i + 1
            # and the sequence to be read always starts with the key
         {
             assert(_i < NF, "_i < NF");

             if (fields_already_read[_i + 1] > 0)
                # this implies that we are reading an array
             {
                assert(fields_already_read[_i + 1] \
                       != field_read_not_in_array,
                       "fields_already_read[_i + 1] != field_read_not_in_array");

                _which_array = fields_already_read[_i + 1];

                for (_k = 0; _k <= arrays_read[_which_array]["array"][0];
                     _k++)
                {
                    temp_array[$_i][_k] = arrays_read[_which_array]["array"][_k];
                }

                _nb_of_fields_in_array                                  \
                    = arrays_read[_which_array]["nb_fields_read"];

                _i += _nb_of_fields_in_array - 1;
            }
            else
            {
                temp_array[$_i] = $(_i + 1);
                _i++;
            }
        }
    }
}


function update_changed_field(temp_array, previous_version_events, entity_uuid, field)
{
    if (isarray(temp_array[field]) \
        && temp_array[field]["changed"])
        # there shouldn't be a misunderstanding here:
        # previous_version_events reflects the actual
        # value/state of the entities, completely ignoring
        # if it originates from add/edit/delete
        # ie. we simply couldn't care less how these values
        # came about in the PREVIOUS version
    {
        if (isarray(temp_array[field]["value"]))
        {
            for (_k = 0; _k <= temp_array[field]["value"][0];
                 _k++)
            {
                previous_version_events[entity_uuid][field][_k] \
                    = temp_array[field]["value"][_k];
            }
        }
        else
        {
            previous_version_events[entity_uuid][field] = \
                temp_array[field]["value"];
        }
    }
}

function process_events_from_history_file_and_build_uuid_tree(previous_version_events,
                                                              uuid_tree,
                                                              event_classes,
                                                              _i, _j, _k,
                                                              _temp_array,
                                                              _fields_already_read,
                                                              _field_read_not_in_array,
                                                              _arrays_read,
                                                              _field, 
                                                              _event_type,
                                                              _entity_uuid,
                                                              _trimmed_entity_uuid,
                                                              _parent_uuid,
                                                              _old_parent_uuid,
                                                              _new_parent_uuid,
                                                              _dump_uuid)
# global args not declared in the profile are: $0, NF
#
# in awk all vars are global, except when we make variables local
# to functions
#
# typically I declare all global vars used in the function in the profile
# for legibility
{
#    print "";
 #   print "start";
  #  print $0;

    # this will be required to monitor what has been read from $0 so far
    # we'll need 3 passes to do so
    split ("", _fields_already_read);
    for (_i = 1; _i <= NF; _i++)
    {
        _fields_already_read[_i] = 0;
    }
    _field_read_not_in_array = NF + 1;
    
    split("", _arrays_read);
    _nb_of_arrays_read = 0;
    
    # let's do a first pass to read out arrays, if any
    process_events_from_history_file_pass_1(_arrays_read,
                                            _fields_already_read);

    #_temp_array to gather what is being read from $0
    split ("", _temp_array);
    # a second pass to deal with edit events
    process_events_from_history_file_pass_2(_arrays_read,
                                            _fields_already_read,
                                            _field_read_not_in_array,
                                            _temp_array);

    process_events_from_history_file_pass_3(_arrays_read,
                                            _fields_already_read,
                                            _field_read_not_in_array,
                                            _temp_array);

    _event_type = _temp_array["\"eventType\""];
    _entity_uuid = _temp_array["\"entityUuid\""];
    _trimmed_entity_uuid = strip_quotes_from_string(_entity_uuid);
   
    previous_version_events[_trimmed_entity_uuid]["flagged_for_possible_removal_of_subtree"] = 0;

#    if (_trimmed_entity_uuid == "f8228b30-c97b-418b-a227-dda4e61bd131")
#    {
#        print $0;
#        print ">>> in _TEMP_ARRAY";
#        for (_f in _temp_array)
#        {
#            if (isarray(_temp_array[_f]))
#            {
#                print _f;
#                print_array(_temp_array[_f]);
#            }
#            else
#            {
#                print _f, _temp_array[_f];
#            }
#        }

#        print_dump_uuid(">>> after 3rd pass in PREV ", previous_version_events, _trimmed_entity_uuid);
#    }

    if (_event_type in event_classes["add"])
    {
        for (_field in _temp_array)
        {
            if (isarray(_temp_array[_field]))
            {
                for (_k = 0; _k <= _temp_array[_field][0]; _k++)
                {
                    previous_version_events[_trimmed_entity_uuid][_field][_k] \
                        = _temp_array[_field][_k];
                }
            }
            else
            {
                previous_version_events[_trimmed_entity_uuid][_field]   \
                    = _temp_array[_field];
            }
        }
        previous_version_events[_trimmed_entity_uuid]["\"objectType\""] \
            = event_type_to_object_type(_event_type);

        # I might have to add some fields which are getting added as the metaModel version
        # is updated
        # this needs a thoughrough treatment, but for now dirty stuff will have to suffice

        if (previous_version_events[_trimmed_entity_uuid]["\"objectType\""] == "\"Question\"" \
            && previous_version_events[_trimmed_entity_uuid]["\"questionType\""] == "\"ListQuestion\"")
        {
            if (!("\"itemTemplateQuestionUuids\"" in previous_version_events[_trimmed_entity_uuid]))
            {
                previous_version_events[_trimmed_entity_uuid]["\"itemTemplateQuestionUuids\""][0] = 0;
            }
        }
    }
    else if (_event_type in event_classes["edit"])
    {
        # the file read here line by line
        # is ordered wrt to package version numbers

        # so the entiry_uuid must already have been encountered before,
        # and must be recorded in previous_version_events

        assert(_trimmed_entity_uuid in previous_version_events,
               "_trimmed_entity_uuid in previous_version_events");
        
        for (_field in previous_version_events[_trimmed_entity_uuid])
        {
            # only the fields have changed get updated
            update_changed_field(_temp_array, previous_version_events, _trimmed_entity_uuid, _field);
        }
        # note here this isn't quite sufficient since
        # as apparently there are fields in the edit version
        # that do not appear in the add version...
        # actually, not sure I understand... NEEDS more work
        # ... here we go...
        # a first try
        for (_field in _temp_array)
        {
            if (!(_field in previous_version_events[_trimmed_entity_uuid]))
            {
                if (isarray(_temp_array[_field])  \
                    && _temp_array[_field]["changed"])
                {
                    update_changed_field(_temp_array, previous_version_events, _trimmed_entity_uuid, _field);
                }
                else if (isarray(_temp_array[_field]))
                {
                    for (_k = 0; _k <= _temp_array[_field][0];
                         _k++)
                    {
                        previous_version_events[_trimmed_entity_uuid][_field][_k] \
                            = _temp_array[_field][_k];
                    }
                    # delete previous_version_events[_trimmed_entity_uuid][_field];
                    # deep_array_copy_to(_temp_array[_field],
                    #                    previous_version_events[_trimmed_entity_uuid][_field]);
                }
                else
                {
                    previous_version_events[_trimmed_entity_uuid][_field] = _temp_array[_field];
                }
            }
        }

#        if (_trimmed_entity_uuid == "f8228b30-c97b-418b-a227-dda4e61bd131")
#        {
#            print_dump_uuid(">>> after EDIT update in PREV ", previous_version_events, _trimmed_entity_uuid);
#        }
        
        previous_version_events[_trimmed_entity_uuid]["\"objectType\""] \
            = event_type_to_object_type(_event_type);
        
        if (event_type_to_object_type(_event_type) == "\"Question\"")
            # bloody question must be handled differently
            # will explain why later
        {
            if (previous_version_events[_trimmed_entity_uuid]["\"questionType\""] \
                != _temp_array["\"questionType\""])
            {
             #   if (_trimmed_entity_uuid == "1ae951ad-a3c2-4963-aad4-c2a1577c6491")
              #  {
               # print "we are changing question type for", _trimmed_entity_uuid;
              #  print "in list", previous_version_events[_trimmed_entity_uuid]["\"questionType\""];
               # print "in temp", _temp_array["\"questionType\""];
               # }
                
                previous_version_events[_trimmed_entity_uuid]["\"questionType\""] \
                = _temp_array["\"questionType\""];

                # special treatment for change of question type:
                # mark this entity to possibly remove its children
                # we'll do this once all the entities move has been processed
                #
                previous_version_events[_trimmed_entity_uuid]["flagged_for_possible_removal_of_subtree"] = 1;

                # and add a field if new type is ListQuestion
                if (previous_version_events[_trimmed_entity_uuid]["\"questionType\""] == "\"ListQuestion\"")
                {
                    assert(!("\"itemTemplateQuestionUuids\"" in previous_version_events[_trimmed_entity_uuid]), \
                           "itemTemplateQuestionUuids NOT in previous_version_events[_trimmed_entity_uuid]");
                    previous_version_events[_trimmed_entity_uuid]["\"itemTemplateQuestionUuids\""][0] = 0;
                }
            }
        }
    }
    else if (_event_type in event_classes["move"])
    {
        # the file read here line by line
        # is ordered wrt to package version numbers

        # so the entiry_uuid must already have been encountered before,
        # and must be recorded in previous_version_events
        assert(_trimmed_entity_uuid in previous_version_events,
               "_trimmed_entity_uuid in previous_version_events");

        # this is previous parent
        _old_parent_uuid = _temp_array["\"parentUuid\""];
        # and this is the new parent
        _new_parent_uuid = _temp_array["\"targetUuid\""];

        # both uuids must already exist (for the same reason as mentioned
        # above)
#        print "entity", _trimmed_entity_uuid, "old_parent", strip_quotes_from_string(_old_parent_uuid);

        assert(strip_quotes_from_string(_old_parent_uuid) in previous_version_events,
               "strip_quotes_from_string(_old_parent_uuid) in previous_version_events");
        assert(strip_quotes_from_string(_new_parent_uuid) in previous_version_events,
               "strip_quotes_from_string(_new_parent_uuid) in previous_version_events");
        # we simply change the parent uuid for this entity
    
        previous_version_events[_trimmed_entity_uuid]["\"parentUuid\""] = \
            _new_parent_uuid;

        # ha! and modify uuid_tree
        move_subtree_in_tree(uuid_tree,
                             strip_quotes_from_string(_old_parent_uuid),
                             strip_quotes_from_string(_new_parent_uuid),
                             _trimmed_entity_uuid);

  #      if (_trimmed_entity_uuid == "1ae951ad-a3c2-4963-aad4-c2a1577c6491")
   #     {
    #        print "children of", strip_quotes_from_string(_old_parent_uuid);
     #       for (_temp_uuid in uuid_tree[strip_quotes_from_string(_old_parent_uuid)])
      #          print _temp_uuid;
       #     print "children of", strip_quotes_from_string(_new_parent_uuid);
      #      for (_temp_uuid in uuid_tree[strip_quotes_from_string(_new_parent_uuid)])
       #         print _temp_uuid;
        #}
    }
    else
    {
        assert(_event_type in event_classes["delete"],
               "_event_type in event_classes[\"delete\"]");
        delete previous_version_events[_trimmed_entity_uuid];
    }

    # update the uuid tree, but only in one case: when there is a new uuid to take care of
    # edit events: there is nothing to do
    # move events: already taken care of
    if (_event_type in event_classes["add"])
    {
        _parent_uuid = _temp_array["\"parentUuid\""];
        if (_parent_uuid != "\"00000000-0000-0000-0000-000000000000\"")
        {
            uuid_tree[strip_quotes_from_string(_parent_uuid)][_trimmed_entity_uuid] = 1;
        }
    }

#    if (_trimmed_entity_uuid == "1ae951ad-a3c2-4963-aad4-c2a1577c6491")
 #   {
  #      print_dump_uuid("end of reading history", previous_version_events, _trimmed_entity_uuid);
   # }
}


function is_child_compatible_with_parent(list_of_events, parent_uuid, child_uuid,
                                         _parent_question_type, _child_object_type)
# has to do with changing question type
# must explain
{
    assert(list_of_events[parent_uuid]["\"questionType\""] != "", 
           "list_of_events[parent_uuid][questionType] != nothing");
    # otherwise we wouldn't event be in this case
    # can only end up here if there was a change of question type
    _parent_question_type = list_of_events[parent_uuid]["\"questionType\""];
    _child_object_type = list_of_events[child_uuid]["\"objectType\""];

    #print "parent", parent, _parent_question_type, "child", child, _child_object_type;
    switch (_child_object_type)
    {
        case "\"Choice\"":
        {
            if (_parent_question_type == "\"MultiChoiceQuestion\"")
            {
                return 1;
            }
            return 0;
        }
        case "\"Answer\"":
        {
            if (_parent_question_type == "\"OptionsQuestion\"")
            {
                return 1;
            }
            return 0;
        }
        case "\"Question\"":
        {
            if (_parent_question_type == "\"ListQuestion\"")
            {
                return 1;
            }
            return 0;
        }
        default:
            return 0;
    }
}

function align_list_of_events_and_uuid_tree(list_of_events, uuid_tree, 
                                            _uuid, _child, _list_to_consider, _list_to_remove)
{
    for (_uuid in uuid_tree)
    {
        if (! _uuid in list_of_events)
        {
            remove_orphans_from_list_and_tree(uuid_tree, list_of_events, _uuid);
        }
    }

    split("", _list_to_consider);
    for (_uuid in list_of_events)
    {
        if (list_of_events[_uuid]["flagged_for_possible_removal_of_subtree"])
        {
            _list_to_consider[_uuid] = 1;
        }
    }
    
    split("", _list_to_remove);
    # note: I can't do a loop as 'for uuid in list_of_events'
    # since:
    # the loop creates a list of items to visit ahead of loop execution
    #
            # As a point of information, gawk sets up the list of elements
            # to be iterated over before the loop starts, and does not
            # change it. But not all awk versions do so
    # for uuid1, uuid2, uuid3, etc...
    #     now, assume that inside 'for uuid2', the child uuid3 get's removed
    #     then, unfortunately, as soon as the loop proceeds to
    #     'for uuid3 in list'.... well, this adds uuid3 back in the list...
    #     which is obviously not the desired outcome
    #
    #
    # we use a _list_to_remove for the same reason
    for (_uuid in _list_to_consider)
    {
        if (_uuid in uuid_tree)
        {
            for (_child in uuid_tree[_uuid])
            {
                if (!(is_child_compatible_with_parent(list_of_events, _uuid, _child)))
                {
                    #print "in alignment, delete", _child;
                    remove_orphans_from_list_and_tree(uuid_tree, list_of_events, _child);
                    _list_to_remove[_child] = 1;
                }
                else
                {
                    list_of_events[_uuid]["flagged_for_possible_removal_of_subtree"] = 0;
                }
            }
        }
    }
    
    # I have to this because each time there is a mention to uuid_tree[_uuid] or list_of_events[_uuid]
    # after they have been deleted, it creates anew elt in those lists
    for (_uuid in _list_to_remove)
    {
        delete list_of_events[_uuid];
        delete uuid_tree[_uuid];
    }
}


function remove_orphans_from_list_and_tree(uuid_tree, list_of_events, uuid,
                                  _child)
{
    if (uuid in uuid_tree)
    {
        for (_child in uuid_tree[uuid])
        {
            # note:
            # As a point of information, gawk sets up the list of elements
            # to be iterated over before the loop starts, and does not
            # change it. But not all awk versions do so
            #
            # why this comment?
            # --> to make sure that each child is only seen once
            # in this loop
            # it is a silly question, still I was pondering...
            
            if (_child in list_of_events)
            {
                remove_orphans_from_list_and_tree(uuid_tree,
                                         list_of_events, _child);
                delete list_of_events[_child];
            }
            delete uuid_tree[_child];
        }
    }
    # uuid is not in the tree (so cannot have children anyway)
    # or uuid has no children any longer in uuid_tree: end of recursion
    return;
}
function move_subtree_in_tree(uuid_tree, old_parent_uuid, new_parent_uuid,
                              entity_uuid,
                              _child)
{
    assert(old_parent_uuid in uuid_tree, "old_parent_uuid in uuid_tree");
    # uuid_tree is only indexed by non leaves of the tree

    # so it is possible that new_parent is a leaf and might
    # not be index the tree

    for (_child in uuid_tree[old_parent_uuid])
    {
        if (_child == entity_uuid)
        {
            delete uuid_tree[old_parent_uuid][_child];
        }
    }
    uuid_tree[new_parent_uuid][entity_uuid] = 1;
}

function print_array(arr, _f)
{
    assert(isarray(arr), "isarray(arr)");
    for (_f in arr)
    {
        print _f, arr[_f];
    }
}
function print_dump_uuid(str, list, uuid,     _field, _f, _ff)
{
    print str;
    for (_field in list[uuid])
    {
        if (! isarray(list[uuid][_field]))
        {
            print _field, list[uuid][_field];
        }
        else
        {
            print _field;
            for (_f in list[uuid][_field])
            {
                if (! isarray(list[uuid][_field][_f]))
                {
                    print _f, list[uuid][_field][_f];
                }
                else
                {
                    for (_ff in list[uuid][_field][_f])
                    {
                        print _f, _ff, list[uuid][_field][_f][_ff];
                    }            
                }
            }
        }     
    }
}

function arrays_are_identical(array_a, array_b,  _i)
{
    # do they have equal size?
    if (array_a[0] != array_b[0])
    {
        return 0;
    }

    for (_i = 1; _i <= array_a[0]; _i++)
    {
        if (array_a[_i] != array_b[_i])
        {
            return 0;
        }
    }
    return 1;
}

function field_values_are_identical(field_a, field_b)
{
    if (isarray(field_a))
    {
        assert(isarray(field_b), "isarray(field_b)");
        return arrays_are_identical(field_a, field_b); 
    }
    else if (isarray(field_b))
    {
        return 0;
    }
    else if (field_a == field_b)
    {
        return 1;
    }
    return 0;
}


    
function compare_events_between_versions(previous_version_events,
                                         this_version_events,
                                         this_version_move_events,
                                         uuid_tree,
                                         this_version_events_order,
                                         _entity_uuid,
                                         _new_parent_uuid,
                                         _stripped_new_parent_uuid,
                                         _has_duplicate,
                                         _old_parent_uuid,
                                         _object_type,
                                         _question_type,
                                         _event_type,
                                         _non_edit_fields,
                                         _fields,
                                         _field,
                                         _value,
                                         _value_array,
                                         _array_size,
                                         _question_type_change,
                                         _edited,
                                         _moved,
                                         _temp_this_version_events)
{
    split("", _non_edit_fields);
    non_editable_fields(_non_edit_fields);
    
    # first bit of symmetric difference
    # note that it is very important to process these steps below
    # in the order given.... as this_version_events will be modified here,
    # as a consequence, 
    # we'll need a temp array  _temp_this_version_events
    # otherwise we would process elts here twice...
    for (_entity_uuid in previous_version_events)
    {
        if (!(_entity_uuid in this_version_events))
        {
            _object_type \
                = previous_version_events[_entity_uuid]["\"objectType\""];
            _event_type = object_to_delete_action(_object_type);

            _temp_this_version_events[_entity_uuid]["\"eventType\""]     \
                = _event_type;
            for (_field in _non_edit_fields)
            {
                _temp_this_version_events[_entity_uuid][_field] \
                    = previous_version_events[_entity_uuid][_field];
            }
        }
    }

    for (_entity_uuid in this_version_events)
    {
        # intersection previous/current
        if (_entity_uuid in previous_version_events)
        {
            _object_type = this_version_events[_entity_uuid]["\"objectType\""];
            _question_type_change = 0;
            _edited = 0;
            _moved = 0;

            if (_object_type == "\"Question\"")
            {
                assert("\"questionType\"" in this_version_events[_entity_uuid],
                       "questionType in this_version_events[_entity_uuid]");
                assert("\"questionType\"" in previous_version_events[_entity_uuid],
                       "questionType in previous_version_events[_entity_uuid]");
                        
                if (previous_version_events[_entity_uuid]["\"questionType\""] \
                    != this_version_events[_entity_uuid]["\"questionType\""])
                {
                    _question_type_change = 1;
                    _edited = 1;
                }
            }
            
            # so, note that here a elt of this_version_events
            # can combine a move and an edit event
            # why? because we are reading from a csv file,
            # and a row may be modified in different ways:
            # - changing its parent
            # - and modifying other fields
            #
            # this cannot happen when reading 'previous_events',
            # which are constructed from dsw packages which
            # we must suppose to be properly created/formed
            # ie
            # a move event is a discrete event
            #
            # or:
            # if a record is moved and then modified (or vice-versa)
            # in the same version, then
            # we'll always have
            # - one event for the move
            # - and as many events for the other edits
            #
            # but as said, this is not visible when reading
            # a csv row describing one event
            #
            # therefore, here, if a row of this_version_events
            # has evidence of a move and an edit, we'll have to create
            # a supplementary row to account for the discrete move event
            for (_field in this_version_events[_entity_uuid])
            {
                if (_field != "\"uuid\"")
                    # actually this field is attributed
                    # automatically to each event
                    # and is not saved, so is not concerned
                    # by this comparison here:
                    # recall that is is different from entityUuid
                {
                    if (_field == "\"itemTemplateQuestionUuids\"")
                    {
                        if (!(_question_type_change))
                        {
                            assert(isarray(this_version_events[_entity_uuid][_field]), \
                                   "isarray this_version_events[_entity_uuid][_field]");
                            assert(isarray(previous_version_events[_entity_uuid][_field]), \
                                   "isarray previous_version_events[_entity_uuid][_field]");
                            
                            if (!(field_values_are_identical(this_version_events[_entity_uuid][_field],
                                                          previous_version_events[_entity_uuid][_field])))
                            {
                                _edited = 1;
                            }
                        }
                    }
                    else if (!(field_values_are_identical(this_version_events[_entity_uuid][_field],
                                                          previous_version_events[_entity_uuid][_field])))
                    {
                        #print "_field", _field, "_question_type_change", _question_type_change;
                        #print_dump_uuid("prev", previous_version_events, _entity_uuid);
                        #print_dump_uuid("this", this_version_events, _entity_uuid);

                        if (_field == "\"parentUuid\"")
                            # this is a move event
                        {
                            #print_dump_uuid("prev", previous_version_events, _entity_uuid);
                            #print_dump_uuid("this", this_version_events, _entity_uuid);

                            #print _entity_uuid, _object_type;
                            
                            assert(_object_type == "\"Question\""       \
                                   || _object_type == "\"Answer\"",
                                   "_object_type == Question \
                                   || _object_type == Answer");
                            # because those are the only two cases
                            # I am handling up to now
                            
                            _moved = 1;
                        }
                        else
                            # it is en edit event
                        {
                            _edited = 1;
                        }
                    }
                }
            }
           
            if (! (_edited || _moved))
                # in which case, the item is simply removed
                # from this_version_events
                #
                # since it is already present in previous_version_events
                # ie, taken care of by the previous package bundle
                # (for this to make sense, must describe how dsw package bundle
                # works...)
            {
                delete this_version_events[_entity_uuid];
            }

            _new_parent_uuid = 0;
            _old_parent_uuid = 0;
            if (_edited)
            {
                # for now we concentrate on the edit event
                _event_type = object_to_edit_action(_object_type);

                this_version_events[_entity_uuid]["\"eventType\""]     \
                    = _event_type;
                
                _question_type = "";
                if ("\"questionType\"" in this_version_events[_entity_uuid])
                {
                    _question_type = this_version_events[_entity_uuid]["\"questionType\""];
                }
                split("", _fields);
                event_type_to_fields(_event_type,
                                     _question_type,
                                     _fields);
                # the above: careful
                # if I were to do event_type_to_fields(_event_type,
                #                     this_version_events[_entity_uuid]["\"questionType\""],
                #                     _fields);
                # I would simply add the ["\"questionType\""] field to this_version_events
                # which is absolutely not what I want!!!!
                
                for (_field in _fields)
                {
                    if (_field in _non_edit_fields)
                    {
                        if (_field != "\"uuid\"")
                            # actually this field is attributed
                            # automatically to each event
                            # and is not saved, so is not concerned
                            # by this comparison here:
                            # recall that is is different from entityUuid
                        {
                            if (!(_moved && _field == "\"parentUuid\""))
                            {
                                assert(this_version_events[_entity_uuid][_field] \
                                       == previous_version_events[_entity_uuid][_field],
                                       "this_version_events[_entity_uuid][_field] \
                               == previous_version_events[_entity_uuid][_field]");
                            }
                            else if (_moved && _field == "\"parentUuid\"")
                                # haha!! must restore the previous parent
                                # previous_version_events[_entity_uuid]["\"parentUuid\""]
                                # to this_version_events[_entity_uuid]["\"parentUuid\
                                # since with _moved
                            {
                                _new_parent_uuid \
                                    = this_version_events[_entity_uuid][_field];
                                _old_parent_uuid \
                                    = previous_version_events[_entity_uuid][_field];
                                
                                assert(_new_parent_uuid != _old_parent_uuid,
                                       "_new_parent_uuid != _old_parent_uuid");

                                # restore _old_parent_uuid
                                # we'll create a discrete move event
                                # for this uuid later down
                                this_version_events[_entity_uuid][_field] \
                                    = _old_parent_uuid;
                            }
                        }
                    }
                    else if (_field in this_version_events[_entity_uuid])
                    {
                        if (_field == "\"itemTemplateQuestionUuids\"")
                        {
                            this_version_events[_entity_uuid]["order"] = 1;
                        }
                        
                        if (_field != "\"questionType\"")
                        # for reasons best known to dsw this field's treatment
                        # doesn't seem consistent...

                        # this processing below will also handle
                        # modified questionType: in the sense of automatically
                        # handling all children of the question, if any
                        {
                            if (_field == "\"itemTemplateQuestionUuids\"" \
                                && _question_type_change)
                            {
                                assert(isarray(this_version_events[_entity_uuid][_field]),
                                       "isarray(this_version_events[_entity_uuid][_field]");
                                
                                split("", _value_array);
                                deep_array_copy_to(this_version_events[_entity_uuid][_field], _value_array);
                                delete this_version_events[_entity_uuid][_field];
                                    
                                this_version_events[_entity_uuid][_field]["\"changed\""] \
                                    = "true";
                                for (_k = 0; _k <= _value_array[0]; _k++)
                                {
                                    this_version_events[_entity_uuid][_field]["\"value\""][_k] \
                                        = _value_array[_k];
                                }
                            }
                            else if (field_values_are_identical(this_version_events[_entity_uuid][_field],
                                                                previous_version_events[_entity_uuid][_field]))
                            {
                                delete this_version_events[_entity_uuid][_field];
                                this_version_events[_entity_uuid][_field]["\"changed\""] \
                                    = "false";
                            }
                            else
                            {
                                if (isarray(this_version_events[_entity_uuid][_field]))
                                {
                                    assert(isarray(previous_version_events[_entity_uuid][_field]),
                                           "isarray(previous_version_events[_entity_uuid][_field]");
                                   
                                    split("", _value_array);
                                    deep_array_copy_to(this_version_events[_entity_uuid][_field], _value_array);
                                    delete this_version_events[_entity_uuid][_field];
                                    
                                    this_version_events[_entity_uuid][_field]["\"changed\""] \
                                        = "true";
                                    for (_k = 0; _k <= _value_array[0]; _k++)
                                    {
                                        this_version_events[_entity_uuid][_field]["\"value\""][_k] \
                                            = _value_array[_k];
                                    }
                                }
                                else
                                {
                                    _value = this_version_events[_entity_uuid][_field];
                                    delete this_version_events[_entity_uuid][_field];
                                    
                                   this_version_events[_entity_uuid][_field]["\"changed\""] \
                                       = "true";
                                   this_version_events[_entity_uuid][_field]["\"value\""] \
                                       = _value;
                                }
                            }
                        }
                        else if (this_version_events[_entity_uuid][_field]     \
                                 != previous_version_events[_entity_uuid][_field])
                        {
                            # special treatment for change of question type:
                            # remove all child uuids
                            # otherwise it gets all too confusing
                            # I wish I could explain more...
                            
                            remove_orphans_from_list_and_tree(uuid_tree,
                                                     _temp_this_version_events,
                                                     _entity_uuid);
                        }
                    }
                    else
                    {
                        # these fields appear in the definition of the
                        # edit action, but not as read from the csv template
                        this_version_events[_entity_uuid][_field]["\"changed\""] \
                                    = "false";
                    }
                }
            }

            if (_moved)
            {
                _event_type = object_to_move_action(_object_type);

                #print "move object", _object_type, "event", _event_type;
                this_version_move_events[_entity_uuid]["\"eventType\""]     \
                    = _event_type;

                if (_edited)
                    # we already collected the parent info since it had to be
                    # overwritten
                {
                    assert(_old_parent_uuid != 0 && _new_parent_uuid != 0,
                           "_old_parent_uuid != 0 && _new_parent_uuid != 0");
                }
                else
                {
                    assert(_old_parent_uuid == 0 && _new_parent_uuid == 0,
                           "_old_parent_uuid == 0 && _new_parent_uuid == 0");
                    _new_parent_uuid                            \
                        = this_version_events[_entity_uuid]["\"parentUuid\""];
                    _old_parent_uuid                                    \
                        = previous_version_events[_entity_uuid]["\"parentUuid\""];

                    delete this_version_events[_entity_uuid];
                }
                
                this_version_move_events[_entity_uuid]["\"parentUuid\""] \
                    = _old_parent_uuid;
                this_version_move_events[_entity_uuid]["\"targetUuid\""] \
                    = _new_parent_uuid;
                this_version_move_events[_entity_uuid]["\"entityUuid\""] \
                    = get_quotes_around(_entity_uuid);
                this_version_move_events[_entity_uuid]["\"uuid\""]      \
                    = get_quotes_around(gen_uuid());
            }
        }
        else # _entity_uuid in this_version_events && !_entity_uuid in previous_version_events
            # second bit of symmetric difference
        {
            # entity creation
            # simple create creation eventType according to objectType
            this_version_events[_entity_uuid]["\"eventType\""] \
                = object_to_create_action( \
                    this_version_events[_entity_uuid]["\"objectType\""]);
        }
    }

    
    # now integrate the temp array we created for the first bit of
    # symmetric difference back into the events list
    for (_entity_uuid in _temp_this_version_events)
    {
        for (_field in _temp_this_version_events[_entity_uuid])
        {
            if (isarray(_temp_this_version_events[_entity_uuid][_field]))
            {
                deep_array_copy_to(_temp_this_version_events[_entity_uuid][_field],
                                   this_version_events[_entity_uuid][_field]);
            }
            else
            {
                this_version_events[_entity_uuid][_field]              \
                    = _temp_this_version_events[_entity_uuid][_field];
            }
        }
        this_version_events_order[++this_version_events_order[0]] = _entity_uuid;
    }
}

function write_event(this_version_events,
                     event_uuid,
                     first_record,
                     output_files,
                     _i, _k, _array_size,
                      _event_uuid, _field,
                      _first_field,
                      _changed)
{
    if (!(first_record))
    {
        write_comma(output_files["json_file"]);
    }
    
    printf("\n{") >> output_files["json_file"];
    _first_field = 1;
    
    for (_field in this_version_events[event_uuid])
    {
        if (isarray(this_version_events[event_uuid][_field]))
            # there are two cases where it can be an array
            # - when it is an array [], eg metricMeasures, tagUuis, etc...
            # - and when it is a block {}
            #   to explain this:
            #   in order to handle changed value fields in edit event, I use
            #   an array to deal with this in this_version_events
            #   but in reality, in the json file, this translate into a block (see below)
            #
            #   so, in the custom-designed this_version_events, these two cases are
            #   read as arrays
            
            
            # this is only when we have "changed" fields
        {
            if ("\"changed\"" in this_version_events[event_uuid][_field])
                # we are in case where we are reading an array/block "change", "value"
            {
                if (_first_field)
                {
                    printf("\n%s: {", _field)                   \
                        >> output_files["json_file"];
                    _first_field = 0;
                }
                else
                {
                    printf(",\n%s: {", _field)                  \
                        >> output_files["json_file"];
                }
                
                _changed = this_version_events[event_uuid][_field]["\"changed\""];
                if (_changed == "true")
                {
                    if (isarray(this_version_events[event_uuid][_field]["\"value\""]))
                        # this is when the changed field value is an array []
                    {
                        _array_size = this_version_events[event_uuid][_field]["\"value\""][0];
                        
                        if (_array_size == 0)
                            # array is empty
                        {
                            printf("\n\"value\": [],") >> output_files["json_file"];
                        }
                        else
                        {
                            printf("\n\"value\": [") >> output_files["json_file"];
                            for (_k = 1; _k < _array_size; _k++)
                            {
                                printf("\n%s,", this_version_events[event_uuid][_field]["\"value\""][_k]) \
                                    >> output_files["json_file"];
                            }
                            printf("\n%s", 
                                   this_version_events[event_uuid][_field]["\"value\""][_array_size]) \
                                >> output_files["json_file"];
                            printf("\n],") >> output_files["json_file"];
                        }
                    }
                    else
                    {
                        printf("\n\"value\": %s,",
                               this_version_events[event_uuid][_field]["\"value\""]) \
                            >> output_files["json_file"];
                    }
                }
                printf("\n\"changed\": %s", _changed)           \
                    >> output_files["json_file"];
                printf("\n}") >> output_files["json_file"];
            }
            else
            {
                _array_size = this_version_events[event_uuid][_field][0];
                if (_array_size == 0)
                    # empty array
                {
                    if (_first_field)
                    {
                        printf("\n%s: []", _field)                      \
                            >> output_files["json_file"];
                        _first_field = 0;
                    }
                    else
                    {
                        printf(",\n%s: []", _field)                     \
                            >> output_files["json_file"];
                    }
                }
                else
                {
                    if (_first_field)
                    {
                        printf("\n%s: [", _field)                       \
                            >> output_files["json_file"];
                        _first_field = 0;
                    }
                    else
                    {
                        printf(",\n%s: [", _field)                      \
                            >> output_files["json_file"];
                    }
                    for (_k = 1; _k < _array_size ; _k++)
                    {
                        printf("\n%s,", this_version_events[event_uuid][_field][_k]) \
                            >> output_files["json_file"];
                    }
                    printf("\n%s", this_version_events[event_uuid][_field][_array_size]) \
                        >> output_files["json_file"];
                    printf("\n]") >> output_files["json_file"];
                }
            }
        }
        else
        {                
            if (_first_field)
            {
                printf("\n%s: %s", _field,
                       this_version_events[event_uuid][_field]) \
                    >> output_files["json_file"];
                _first_field = 0;
            }
            else
            {
                printf(",\n%s: %s", _field,
                       this_version_events[event_uuid][_field]) \
                    >> output_files["json_file"];
            }
        }
    }
    printf("\n}") >> output_files["json_file"];
}
        



function write_events(this_version_events_order,
                      this_version_events,
                      this_version_move_events,
                      output_files,
                      _max_events, _i, _k, _array_size,
                      _event_uuid, _field,
                      _first_record, _first_field,
                      _changed)
{
    _max_events = this_version_events_order[0];
    _first_record = 1;

    for (_i = 1; _i <= _max_events; _i++)
    {
        _event_uuid = this_version_events_order[_i];
            
        if (_event_uuid in this_version_events)
        {
            # must clean one field for this record (which is not part
            # of the dsw field list)
            delete this_version_events[_event_uuid]["\"objectType\""];
            delete this_version_events[_event_uuid]["flagged_for_possible_removal_of_subtree"];

            if (this_version_events[_event_uuid]["order"])
            {
                # we'll do this at the very end
            }
            else
            {
                delete this_version_events[_event_uuid]["order"];
                write_event(this_version_events,
                            _event_uuid,
                            _first_record,
                            output_files);
                
                if (_first_record)
                {
                    _first_record = 0;
                }
            }
        }
        
        if (_event_uuid in this_version_move_events)
        {
            # there is no ["\"objectType\""] field in this list
            # I know this is very dirty

            if (_first_record)
            {
                _first_record = 0;
            }
            else
            {
                write_comma(output_files["json_file"]);
            }

            printf("\n{") >> output_files["json_file"];
            _first_field = 1;

            for (_field in this_version_move_events[_event_uuid])
            {
                if (_first_field)
                {
                    printf("\n%s: %s", _field,
                           this_version_move_events[_event_uuid][_field])    \
                        >> output_files["json_file"];
                    _first_field = 0;
                }
                else
                {
                    printf(",\n%s: %s", _field,
                           this_version_move_events[_event_uuid][_field])    \
                        >> output_files["json_file"];
                }
            }
            printf("\n}") >> output_files["json_file"];
        }
    }

    for (_event_uuid in this_version_events)
    {
        if (!(this_version_events[_event_uuid]["order"]))
        {
            continue;
            # we'll do this at the very end
        }
        delete this_version_events[_event_uuid]["order"];
        write_event(this_version_events,
                    _event_uuid,
                    _first_record,
                    output_files);
        if (_first_record)
        {
            _first_record = 0;
        }
    }
}

function array_is_empty(array, _i, _key)
{
    _i = 0;
    for (_key in array)
    {
        _i++;
    }
    if (_i)
    {
        return 0;
    }
    return 1;
}

function dump_list(list, str, output_files,      _i, _uuid, _field)
{
    printf("") > output_files[str];
    for (_uuid in list)
    {
        printf("%s, ", _uuid) >> output_files[str];
        if (isarray(list[_uuid]))
        {
            for (_field in list[_uuid])
            {
                if (! isarray(list[_uuid][_field]))
                {
                    if (_field == "flagged_for_possible_removal_of_subtree")
                    {
                        printf("%s: %d, ", _field, list[_uuid][_field]) >> output_files[str];
                    }
                    else
                    {
                        printf("%s: %s, ", _field, list[_uuid][_field]) >> output_files[str];
                    }
                }
                else
                {
                    if ("\"changed\"" in list[_uuid][_field])
                    {
                        printf("%s: \"changed\":%s,", _field, list[_uuid][_field]["\"changed\""]) >> output_files[str];
                        if ("\"value\"" in list[_uuid][_field])
                        {
                            printf("\"value\":") >> output_files[str];
                            if (isarray(list[_uuid][_field]["\"value\""]))
                            {
                                _array_size = list[_uuid][_field]["\"value\""][0];
                                if (_array_size == 0)
                                {
                                    printf("[],") >> output_files[str];
                                }
                                else
                                {
                                    printf("[") >> output_files[str];
                                    printf("%s", list[_uuid][_field]["\"value\""][1]) >> output_files[str];
                                    for (_i = 2; _i <= list[_uuid][_field][0]; _i++)
                                    {
                                        printf (", %s", list[_uuid][_field][_i]) >> output_files[str];
                                    }
                                    printf(" ],") >> output_files[str];
                                }
                            }
                            else
                            {
                                printf("%s,", list[_uuid][_field]["\"value\""]) >> output_files[str];
                            }
                        }
                    }
                    else
                    {
                        if (list[_uuid][_field][0] == 0)
                        {
                            printf("%s: [],", _field) >> output_files[str];
                        }
                        else
                        {
                            printf("%s: [", _field) >> output_files[str];
                            printf("%s", list[_uuid][_field][1]) >> output_files[str];
                            for (_i = 2; _i <= list[_uuid][_field][0]; _i++)
                            {
                                printf (", %s", list[_uuid][_field][_i]) >> output_files[str];
                            }
                            printf(" ],") >> output_files[str];
                        }
                    }
                }
            }
        }
        printf("\n") >> output_files[str];
    }
}


BEGIN{
# external variable(s)
    # vkm_id
    # vprevious_version
    # vdebug (0/1)
    # vnchapters
    # vversion
    # vreadme_file
    # vprevious_packs_file

# recall, default FS = " "
    # from https://www.gnu.org/software/gawk/manual/gawk.html#Splitting-By-Content
    # each field is either “anything that is not a comma,” or “a double quote,
    # anything that is not a double quote, and a closing double quote.”
    FPAT = "([^,]*)|(\"[^\"]+\")";

    i = 0;
    max_i = 0;
    nfields = 0; # this will be a constant for all lines

    max_level = 50;
    # this concept of 'level' designates the columns in the csv file
    # that are NOT chaper, text, advice, file, uuid
    # max_level has (theoretically) no upper bound

    max_chapters = 50;
    if (vnchapters == 0)
        # that is, nchapters wasn't initialised via awk command argument
        # passing, so we must set it to a 'big' number
    {
        vnchapters = max_chapters;
    }
    
    split("", previous_version_events);
    split("", event_classes);
    classify_event_type(event_classes);
    split("", uuid_tree);
}
{
    if (FNR == NR)
    {
        i++; 
        if (i == 1)
            # this describes the column headings
        {
            nfields = NF;
            
            #print "nfields " nfields;
            #print $0;
            
            # on récupère les indices des column headings
            initialise_header_indexes(max_level, header_array);
            for (j = 1; j <= NF; j++)
            {
                set_header_indexes(header_array,
                                   strip_quotes_from_string($j), j);
            }
        }
        else
        {
            if (NF != nfields)
            {
                print "line", i;
                print $0;
                print "NF " NF;
                print "nfields " nfields;

                for (k = 1; k <= NF; k++)
                    print $k;
            }
            assert(NF == nfields, "NF == nfields");
            
            for (j = 1; j <= NF; j++)
            {
                table[i, j] = strip_quotes_from_string($j);
            }
        }

        number_rows_file_1 = FNR;
    }
    else if (vprevious_version != "none")
    {
        process_events_from_history_file_and_build_uuid_tree(previous_version_events,
                                                             uuid_tree,
                                                             event_classes);
    }
}
END{
    max_row = number_rows_file_1;

    if (vdebug)
    {
        for (i = 1; i <= max_row; i++)
        {
            for (k = 1; k < nfields; k++)
            {
                if (i == 1 && k == 1)
                {
                    printf("%s, ", table[i, k]) > "debug.file";
                }
                else
                {
                    printf("%s, ", table[i, k]) >> "debug.file";
                }
            }
            printf("%s\n", table[i, nfields]) >> "debug.file";
        }
        exit 0;
    }

    # yes, km uuid is harcoded, like actually (almost) all of the uuids
    # this I hope will allow questionnaire migration
    # and importantly, km migration!
    km_uuid = "eda92f2f-62bc-42f7-a88e-b1ffcecf69a5";
    km_name = "Modèle de PGD structure pour la bioimagerie";

    # we need some enums
    # recall that vars are globals in awk by default
    # I write this since the vars below won't be passed
    # explicitely to the fonctions:
    # for once favouring simplicity over legibility
    question_list_of_items = randint(1000000);
    question_options = randint(1000000);
    question_multi_choice = randint(1000000);
    question_value_string  = randint(1000000);
    question_value_number = randint(1000000);
    question_value_date = randint(1000000);
    question_value_text = randint(1000000);
    question_integration = randint(1000000);
    option_answer = randint(1000000);
    choice_answer = randint(1000000);

    # must explain what I am doing here:
    # when a questionType is changing, it becomes necessary to delete its existing children, if any
    # but, since there is no chronological order in the events listed in a package,
    # this can be dangerous: il could be that the children were already moved to another spot
    # so that their removal is irrelevant
    #
    # to counter this problem, we clean the list of events and the tree of events only once
    # (the items to consider would have been flagged at the time of processing)
    align_list_of_events_and_uuid_tree(previous_version_events, uuid_tree);

    split("", this_version_events_order);
    # order_of_events[0] will always contain current number of events
    # processed and included in this_version_events)
    this_version_events_order[0] = 0;

    # this_version_events will store all data relevant to any event
    # processed; the array will be 'indexed' (in the sense of the awk
    # associative arrays) using the entityUuid
    #
    # but, remember that being associative, the associative arrays
    # this_version_events will not be able to list its content
    # in order of creation (which is critical since we are reading
    # the cvs in hierarchical order)
    # -> for this reason, we'll need order_of_events of precisely
    # keep track of the order of creation
    
    # so the multidimentional array this_version_events
    # will hold all events read from the csv template file
    # this will then be compared to events from the previous version
    # which will be stored in a similarly structured array
    
    split("", this_version_events);
    process_events(header_array, table, max_row,
                   km_uuid, km_name,
                   vnchapters, max_level,
                   this_version_events_order,
                   this_version_events);
    
    previous_event_list = "previous_event_list.csv";
    output_files["previous_event_list"] = previous_event_list;
    dump_list(previous_version_events, "previous_event_list", output_files);
    
    current_event_list = "current_event_list.csv";
    output_files["current_event_list"] = current_event_list;
    dump_list(this_version_events, "current_event_list", output_files);

    split("", this_version_move_events);
    compare_events_between_versions(previous_version_events,
                                    this_version_events,
                                    this_version_move_events,
                                    uuid_tree,
                                    this_version_events_order);
    
    current_event_list_after_compare = "current_event_list_after_compare.csv";
    current_move_event_list = "current_move_event_list.csv";

    output_files["current_event_list_after_compare"] = current_event_list_after_compare;
    output_files["current_move_event_list"] = current_move_event_list;

    dump_list(this_version_events, "current_event_list_after_compare", output_files);
    dump_list(this_version_move_events, "current_move_event_list", output_files);

    previous_version = vprevious_version;
    version = vversion;
    kmId = vkm_id;
    if (array_is_empty(this_version_events))
    {
        print "Version " version " appears to show no modification with previous version "\
            previous_version " so will not be created";
        exit 1;
    }
    
    organizationId = "IFB";
    name = "Modèle de PGD structure pour la bioimagerie";
    metamodelVersion = 8;
    
    json_file = kmId"-"version".json";
    print "creating", json_file;
    system("echo -n > "json_file);
    output_files["json_file"] = json_file;

    description = "Projet pilote PGD FBI-EMBRC-IFB";
    license = "CC-BY";

    write_km_starting_tag(output_files);

    write_km_globals(organizationId, name, version, kmId, 
                     metamodelVersion, output_files);

    readme_file = vreadme_file;
    ext = get_file_type(readme_file);
    assert(ext == "md", "ext == \"md\"");

    readme = get_txt_file_content_wblank_lines(readme_file);

    # these two var could be passed as awk parameters if required in the
    # future
    mergeCheckpointPackageId = "null";
    forkOfPackageId = "null";

    if (vprevious_packs_file == "")
    {
        write_packages_starting_tag(output_files);
    }
    else
    {
        system("nl=$(cat "vprevious_packs_file" | wc -l) ; tail -n $(expr $nl - 1) "vprevious_packs_file" |  head -n $(expr $nl - 3)  >> "output_files["json_file"]);

        write_comma(output_files["json_file"]);
    }
    
    write_package_starting_tag(output_files);
    write_package_globals(organizationId, name, version, kmId, 
                          metamodelVersion,
                          license, description,
                          previous_version,
                          mergeCheckpointPackageId, 
                          forkOfPackageId, readme, 
                          output_files);

    write_events_starting_tag(output_files);

    write_events(this_version_events_order,
                 this_version_events,
                 this_version_move_events,
                 output_files);
    
    write_events_ending_tag(output_files);
    write_package_ending_tag(output_files);
    write_packages_ending_tag(output_files);
    write_km_ending_tag(output_files);
}
