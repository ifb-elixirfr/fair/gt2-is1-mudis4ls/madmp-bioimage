BEGIN{
    #input args:  -voutput
    
    FPAT = "([^,]*)|(\"[^\"]+\")";
    i = 0;
}
{
    i++;
    j = 1;
    for (k = 1; k <= NF; k++)
    {
        if ($k ~ "uuid")
        {
            k+=2;
        }
        else
        {
            table[i, j] = $k;
            j++;
        }
    }
}
END{
    max_row = i;
    output_file = voutput;
    for (i = 1; i <= max_row; i++)
    {
        for (j = 1; j < NF; j++)
        {
            if (i == 1 && j == 1)
            {
                printf("%s, ", table[i, j]) > output_file;
            }
            else
            {
                printf("%s, ", table[i, j]) >> output_file;
            }
        }
        printf("%s\n", table[i, NF]) >> output_file;
    }
}
