BEGIN{
    FPAT = "([^,]*)|(\"[^\"]+\")";

    split("", table_events);
    max_i = 0;
    i = 0;
}
{
    i++;

    for (j = 1; j <= NF; j++)
    {
        print $j;
        pair = $j;
        split(pair, a, ":");
        if (a[1] == "\"entityUuid\"")
        {
            uuid = a[2];
            break;
        }
    }

    for (j = 1; j <= NF; j++)
    {
        pair = $j;
        split(pair, a, ":");
        if (a[1] != "\"entityUuid\"")
        {
            table_events[uuid][a[1]] = a[2];
        }
    }
}
END{
    max_i = i;
    for (uuid in table_events)
    {
        print "UUID " uuid;
        for (elt in table_events[uuid])
        {
           print elt, table_events[uuid][elt];
       }
    }
}
